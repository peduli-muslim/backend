package router

import (
	"github.com/MuShaf-NMS/peduli_muslim/config"
	"github.com/MuShaf-NMS/peduli_muslim/handler"
	"github.com/MuShaf-NMS/peduli_muslim/middleware"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func InitializeRouter(server *gin.Engine, db *gorm.DB, appConfig *config.Config) {
	// Repositories
	programRepository := repository.NewProgramRepository(db)
	donaturRepository := repository.NewDonaturRepository(db)
	userRepository := repository.NewUserRepository(db)
	authRepository := repository.NewAuthRepository(db)
	adminRepository := repository.NewAdminRepository(db)
	programImgRepository := repository.NewProgramImgRepository(db)
	articleRepository := repository.NewArticleRepository(db)
	articleImgRepository := repository.NewArticleImgRepository(db)
	blacklistTokenRepository := repository.NewBlacklistTokenRepository(db)

	// Services
	programService := service.NewProgramService(programRepository, programImgRepository)
	donaturService := service.NewDonaturService(donaturRepository, programRepository)
	oAuthService := service.NewOAuthService(config.OAuthConfig)
	userService := service.NewUserService(userRepository)
	authService := service.NewAuthService(authRepository)
	jwtService := service.NewJwtService(appConfig.SecretKey)
	adminService := service.NewAdminService(adminRepository)
	programImgService := service.NewProgramImgService(programImgRepository)
	emailService := service.NewEmailService(*appConfig)
	uploadImgService := service.NewUploadImgService(appConfig.Asstes_Dir, appConfig.Asstes_Link)
	articleImgService := service.NewArticleImgService(articleImgRepository)
	articleService := service.NewArticleService(articleRepository, articleImgRepository)
	blacklistTokenService := service.NewBlacklistTokenService(blacklistTokenRepository)

	// Handlers
	programHandler := handler.NewProgramHandler(programService)
	donaturHandler := handler.NewDonaturHandler(donaturService, userService)
	oAuthHandler := handler.NewOAuthHandler(authService, oAuthService, adminService, userService, jwtService, emailService)
	userHandler := handler.NewUserHandler(userService, authService, jwtService, blacklistTokenService, emailService)
	authHandler := handler.NewAuthHandler(authService, userService, jwtService, blacklistTokenService)
	adminHandler := handler.NewAdminHandler(adminService, authService, jwtService, blacklistTokenService, emailService)
	programImgHandler := handler.NewProgramImgHandler(programImgService, appConfig.Asstes_Dir, appConfig.Asstes_Link)
	uploadImgHandler := handler.NewUploadImgHandler(uploadImgService)
	articleHandler := handler.NewArticleHandler(articleService, articleImgService, appConfig.Asstes_Link)
	articleImgHandler := handler.NewArticleImgHandler(articleImgService, appConfig.Asstes_Dir, appConfig.Asstes_Link)

	// Middlewares
	authMiddleware := middleware.AuthorizeMiddlewareNew()
	localUserMiddleware := middleware.LocalUserMiddlewareNew(jwtService, authService, blacklistTokenService)
	localAdminMiddleware := middleware.LocalAdminMiddlewareNew(jwtService, authService, blacklistTokenService)
	googleUserMiddleware := middleware.GoogleUserMiddlewareNew(oAuthService, authService)
	googleAdminMiddleware := middleware.GoogleAdminMiddlewareNew(oAuthService, authService)
	superAdminMiddleware := middleware.SuperAdminMiddlewareNew(jwtService, adminService, blacklistTokenService)
	refreshUserMiddleware := middleware.RefreshUserMiddlewareNew(jwtService, blacklistTokenService)
	refreshAdminMiddleware := middleware.RefreshAdminMiddlewareNew(jwtService, adminService, blacklistTokenService)

	// Routes
	// server.StaticFS("/assets", http.Dir("assets"))
	programRouter := server.Group("/program")
	{
		programRouter.GET("", programHandler.GetAllPrograms)
		programRouter.POST("", authMiddleware, localAdminMiddleware, googleAdminMiddleware, programHandler.CreateProgram)
		programRouter.GET("/:id", programHandler.GetOneProgram)
		programRouter.PUT("/:id", authMiddleware, localAdminMiddleware, googleAdminMiddleware, programHandler.UpdateProgram)
		programRouter.GET("/status/:id", authMiddleware, localAdminMiddleware, googleAdminMiddleware, programHandler.ToggleStatus)
		programRouter.DELETE("/:id", authMiddleware, localAdminMiddleware, googleAdminMiddleware, programHandler.DeleteProgram)
	}
	donaturRouter := server.Group("/donatur")
	{
		donaturRouter.POST("/:programID", authMiddleware, localUserMiddleware, googleUserMiddleware, donaturHandler.CreateDonatur)
		donaturRouter.GET("/:programID", donaturHandler.GetAllbyProgram)
		donaturRouter.POST("/verifikasi", donaturHandler.VerifikasiPembayaran)
	}
	oAuthRouter := server.Group("/google")
	{
		oAuthRouter.POST("/token-user", oAuthHandler.GetTokenUser)
		oAuthRouter.POST("/token-admin", oAuthHandler.GetTokenAdmin)
	}
	userRouter := server.Group("/user")
	{
		userRouter.POST("/register", userHandler.CreateUser)
		userRouter.POST("/login", userHandler.Login)
		userRouter.GET("/profile", authMiddleware, localUserMiddleware, googleUserMiddleware, userHandler.GetOneUser)
		userRouter.PUT("", authMiddleware, localUserMiddleware, googleUserMiddleware, userHandler.UpdateUser)
		userRouter.GET("/logout", authMiddleware, localUserMiddleware, userHandler.Logout)
		userRouter.GET("/refresh", authMiddleware, localUserMiddleware, refreshUserMiddleware, userHandler.Refresh)
	}
	authRouter := server.Group("/auth", authMiddleware)
	{
		authRouter.GET("", authHandler.GetAuth)
		authRouter.PUT("/user/username", localUserMiddleware, googleUserMiddleware, authHandler.UpdateUsernameUser)
		authRouter.PUT("/user/password", localUserMiddleware, googleUserMiddleware, authHandler.UpdatePasswordUser)
		authRouter.PUT("/admin/username", localAdminMiddleware, googleAdminMiddleware, authHandler.UpdateUsernameAdmin)
		authRouter.PUT("/admin/password", localAdminMiddleware, googleAdminMiddleware, authHandler.UpdatePasswordAdmin)
	}
	adminRouter := server.Group("admin")
	{
		adminRouter.POST("/register", authMiddleware, localAdminMiddleware, googleAdminMiddleware, superAdminMiddleware, adminHandler.CreateAdmin)
		// adminRouter.POST("/register", adminHandler.CreateAdmin)
		adminRouter.POST("/login", adminHandler.Login)
		adminRouter.GET("/logout", authMiddleware, localAdminMiddleware, googleAdminMiddleware, adminHandler.Logout)
		adminRouter.GET("/:uuid", authMiddleware, localAdminMiddleware, googleAdminMiddleware, superAdminMiddleware, adminHandler.GetOne)
		adminRouter.PUT("/:uuid", authMiddleware, localAdminMiddleware, googleAdminMiddleware, superAdminMiddleware, adminHandler.UpdateAdmin)
		adminRouter.GET("", authMiddleware, localAdminMiddleware, googleAdminMiddleware, superAdminMiddleware, adminHandler.GetAll)
		adminRouter.GET("/profile", authMiddleware, localAdminMiddleware, googleAdminMiddleware, adminHandler.GetOneSelf)
		adminRouter.PUT("/profile", authMiddleware, localAdminMiddleware, googleAdminMiddleware, adminHandler.UpdateAdminSelf)
		adminRouter.GET("/refresh", authMiddleware, localAdminMiddleware, refreshAdminMiddleware, adminHandler.Refresh)
	}
	programImgRouter := server.Group("program/image")
	{
		programImgRouter.POST("/add", authMiddleware, localAdminMiddleware, googleAdminMiddleware, programImgHandler.AddImage)
	}
	uploadImgRouter := server.Group("upload")
	{
		uploadImgRouter.POST("/image", authMiddleware, localAdminMiddleware, googleAdminMiddleware, uploadImgHandler.Upload)
	}
	articleRouter := server.Group("article")
	{
		articleRouter.POST("", authMiddleware, localAdminMiddleware, googleAdminMiddleware, articleHandler.CreateArticle)
		articleRouter.GET("", articleHandler.GetAll)
		articleRouter.GET("/:uuid", articleHandler.GetOne)
		articleRouter.PUT("/:uuid", authMiddleware, localAdminMiddleware, googleAdminMiddleware, articleHandler.Update)
	}
	articleImgRouter := server.Group("article/image")
	{
		articleImgRouter.POST("add", authMiddleware, localAdminMiddleware, googleAdminMiddleware, articleImgHandler.AddImage)
	}
}
