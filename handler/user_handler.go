package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

type UserHandler interface {
	CreateUser(ctx *gin.Context)
	GetOneUser(ctx *gin.Context)
	UpdateUser(ctx *gin.Context)
	Login(ctx *gin.Context)
	Logout(ctx *gin.Context)
	Refresh(ctx *gin.Context)
}

type userHandler struct {
	userService           service.UserService
	authService           service.AuthService
	jwtService            service.JwtSerivce
	blacklistTokenService service.BlacklistTokenService
	emailService          service.EmailService
}

func (ah *userHandler) CreateUser(ctx *gin.Context) {
	var user dto.CreateUser
	ctx.BindJSON(&user)
	if err := helper.Validate(&user); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if userExist := ah.authService.CheckUser(user.Username); userExist {
		res := helper.ErrorResponseBuilder("User exist", nil)
		ctx.JSON(400, res)
		return
	}
	createdUser, err := ah.userService.CreateUser(user)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	auth := dto.Register{
		UserID:   createdUser.UUID,
		Username: user.Username,
		Password: user.Password,
		// Email:    user.Email,
	}
	fmt.Println("AUTH")
	fmt.Println(auth)
	// auth.UserID = createdUser.UUID
	if _, err = ah.authService.CreateAuth(auth); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	access_token := ah.jwtService.GenerateToken(auth.UserID, "user", "access_token")
	refresh_token := ah.jwtService.GenerateToken(auth.UserID, "user", "refresh_token")
	data := map[string]interface{}{
		"access_token":  access_token,
		"refresh_token": refresh_token,
		"user":          createdUser,
	}
	res := helper.ResponseBuilder("Register success", data)
	ctx.JSON(200, res)
}

func (uh *userHandler) GetOneUser(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			user, err := uh.userService.GetOneUser(userID)
			if err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			res := helper.ResponseBuilder("Get User Success", user)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
}

func (uh *userHandler) UpdateUser(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			var user dto.UpdateUser
			ctx.BindJSON(&user)
			fmt.Println(user)
			if err := helper.Validate(user); err != nil {
				errs := helper.ValidationError(err)
				res := helper.ErrorResponseBuilder("Validation error", errs)
				ctx.JSON(400, res)
				return
			}
			updatedUser, err := uh.userService.UpdateUser(user, userID)
			if err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			res := helper.ResponseBuilder("Update User Success", updatedUser)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
}

func (ah *userHandler) Login(ctx *gin.Context) {
	var login dto.Login
	ctx.BindJSON(&login)
	err := helper.Validate(&login)
	if err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	auth, err := ah.authService.LoginUser(login)
	// auth, err := ah.authService.Login(login)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	// user, _ := ah.userService.GetOneUser(auth.UserID)
	data := map[string]interface{}{
		"user": auth,
	}
	data["access_token"] = ah.jwtService.GenerateToken(auth.UserID, "user", "access_token")
	data["refresh_token"] = nil
	if login.RememberMe {
		data["refresh_token"] = ah.jwtService.GenerateToken(auth.UserID, "user", "refresh_token")
	}
	res := helper.ResponseBuilder("Login success", data)
	ctx.JSON(200, res)
}

func (ah *userHandler) Logout(ctx *gin.Context) {
	if getTokenClaim, ok := ctx.Get("tokenClaim"); ok {
		if tokenClaim, ok := getTokenClaim.(jwt.MapClaims); ok {
			ah.blacklistTokenService.CreateBlacklistToken(tokenClaim["jti"].(string))
			res := helper.ResponseBuilder("Logout success", nil)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *userHandler) Refresh(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			accessToken := ah.jwtService.GenerateToken(userID, "user", "access_token")
			res := helper.ResponseBuilder("Refresh token success", map[string]interface{}{
				"access_token": accessToken,
			})
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func NewUserHandler(userService service.UserService, authService service.AuthService, jwtService service.JwtSerivce, blacklistTokenService service.BlacklistTokenService, emailService service.EmailService) UserHandler {
	return &userHandler{
		userService:           userService,
		authService:           authService,
		jwtService:            jwtService,
		blacklistTokenService: blacklistTokenService,
		emailService:          emailService,
	}
}
