package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
)

type OAuthHandler interface {
	GetTokenAdmin(ctx *gin.Context)
	GetTokenUser(ctx *gin.Context)
}

type oAuthHandler struct {
	authService  service.AuthService
	oAuthService service.OAuthService
	adminService service.AdminService
	userService  service.UserService
	jwtService   service.JwtSerivce
	emailService service.EmailService
}

func (oah *oAuthHandler) GetTokenAdmin(ctx *gin.Context) {
	var code dto.Code
	ctx.BindJSON(&code)
	if err := helper.Validate(&code); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	token, err := oah.oAuthService.GetToken(ctx, code.Code)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	userInfo, err := oah.oAuthService.GetUserInfo(token.AccessToken)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	auth, err := oah.authService.GetAdminByEmail(userInfo.Email)
	if err != nil {
		res := helper.ErrorResponseBuilder("Access denied", nil)
		ctx.JSON(403, res)
		return
	}
	if auth.GID == "" {
		oah.adminService.SetGIDNama(userInfo.ID, userInfo.Name, auth.UserID)
		auth.GID = userInfo.ID
		auth.Nama = userInfo.Name
	}
	data := map[string]interface{}{
		"access_token":  token.AccessToken,
		"refresh_token": token.RefreshToken,
	}
	fmt.Println(auth)
	data["admin"] = auth
	res := helper.ResponseBuilder("Login Success", data)
	ctx.JSON(200, res)
}

func (oah *oAuthHandler) GetTokenUser(ctx *gin.Context) {
	var code dto.Code
	ctx.BindJSON(&code)
	if err := helper.Validate(&code); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	token, err := oah.oAuthService.GetToken(ctx, code.Code)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	userInfo, err := oah.oAuthService.GetUserInfo(token.AccessToken)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	data := map[string]interface{}{
		"access_token":  token.AccessToken,
		"refresh_token": token.RefreshToken,
	}
	user, err := oah.userService.GetOnebyGID(userInfo.ID)
	if err != nil {
		userCreate := dto.CreateUser{
			GID:   userInfo.ID,
			Nama:  userInfo.Name,
			Email: userInfo.Email,
		}
		user, err := oah.userService.CreateUser(userCreate)
		if err != nil {
			fmt.Println("Create user")
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		// password := helper.PassowrdGenerator(15)
		// fmt.Println(password)
		authCreate := dto.Register{
			UserID: user.UUID,
			// Nama:   user.Nama,
			// Email: user.Email,
		}
		_, err = oah.authService.CreateAuth(authCreate)
		if err != nil {
			fmt.Println("Create auth")
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		// emailBody := fmt.Sprintf("username: <b>%s</b> <br/>password: <b>%s</b>", authCreate.Username, authCreate.Password)
		emailBody := fmt.Sprintf("Wellcome user %s", userCreate.Nama)
		if err := oah.emailService.Send(user.Email, "Registration", emailBody); err != nil {
			e, _ := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		auth, err := oah.authService.GetUser(user.UUID)
		if err != nil {
			fmt.Println("GEt")
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		data["user"] = auth
	} else {
		fmt.Println(user.UUID)
		auth, err := oah.authService.GetUser(user.UUID)
		if err != nil {
			fmt.Println("Get U")
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		data["user"] = auth
	}
	res := helper.ResponseBuilder("Login Success", data)
	ctx.JSON(200, res)
}

func NewOAuthHandler(authService service.AuthService, oAuthService service.OAuthService, adminService service.AdminService, userService service.UserService, jwtService service.JwtSerivce, emailService service.EmailService) OAuthHandler {
	return &oAuthHandler{
		authService:  authService,
		oAuthService: oAuthService,
		adminService: adminService,
		userService:  userService,
		jwtService:   jwtService,
		emailService: emailService,
	}
}
