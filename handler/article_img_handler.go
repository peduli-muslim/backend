package handler

import (
	"fmt"
	"strings"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ArticleImgHandler interface {
	AddImage(ctx *gin.Context)
}

type articleImgHandler struct {
	service    service.ArticleImgService
	assetsDir  string
	assetsLink string
}

func (aih *articleImgHandler) AddImage(ctx *gin.Context) {
	form, err := ctx.MultipartForm()
	if err != nil {
		fmt.Println(err)
		return
	}
	files := form.File["files"]
	res := []map[string]interface{}{}
	for _, file := range files {
		uuid, err := uuid.NewRandom()
		if err != nil {
			fmt.Println(err)
		}
		splitName := strings.Split(file.Filename, ".")
		ext := splitName[len(splitName)-1]
		newname := "img/" + uuid.String() + "." + ext
		path := aih.assetsDir + "/" + newname
		if err := ctx.SaveUploadedFile(file, path); err != nil {
			fmt.Println(err)
			return
		}
		img := dto.AddArticleImage{
			Src:  newname,
			UUID: uuid,
		}
		aih.service.AddImage(img)
		url := aih.assetsLink + "/" + newname
		i := map[string]interface{}{
			"url":  url,
			"uuid": uuid,
		}
		res = append(res, i)
	}
	r := helper.ResponseBuilder("Upload Article Images Success", res)
	ctx.JSON(200, r)
}

func NewArticleImgHandler(service service.ArticleImgService, asstesDir string, asstesLink string) ArticleImgHandler {
	return &articleImgHandler{
		service:    service,
		assetsDir:  asstesDir,
		assetsLink: asstesLink,
	}
}
