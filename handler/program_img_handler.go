package handler

import (
	"fmt"
	"strings"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ProgramImgHandler interface {
	AddImage(ctx *gin.Context)
}

type programImgHandler struct {
	service    service.ProgramImgService
	assetsDir  string
	assetsLink string
}

func (pih *programImgHandler) AddImage(ctx *gin.Context) {
	// file, header, err := ctx.Request.FormFile("file")
	_, header, err := ctx.Request.FormFile("file")
	if err != nil {
		res := helper.ErrorResponseBuilder("Unprocessable entity", nil)
		ctx.JSON(422, res)
		return
	}
	// filename := header.Filename
	// splitName := strings.Split(filename, ".")
	// ext := splitName[len(splitName)-1]
	// fmt.Println(ext)
	// fmt.Println(header.Size)
	// out, err := os.Create(pih.assetsDir + "/" + "img/" + filename)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// defer out.Close()
	// w, err := io.Copy(out, file)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(w)
	// uuid, err := uuid.NewRandom()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// newname := "img/" + uuid.String() + "." + ext
	// absName := pih.assetsDir + "/" + newname
	// fmt.Println(newname)
	// // os.Rename("img/"+filename, newname)
	// os.Rename(pih.assetsDir+"/"+"img/"+filename, absName)
	// // url := "http://192.168.88.246:8000/" + newname
	// imageDto := dto.AddProgramImage{
	// 	Src:  newname,
	// 	UUID: uuid,
	// }
	// _, err = pih.service.AddImage(imageDto)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	uuid, err := uuid.NewRandom()
	if err != nil {
		fmt.Println(err)
	}
	// splitName := strings.Split(file.Filename, ".")
	// ext := splitName[len(splitName)-1]
	// newname := "img/" + uuid.String() + "." + ext
	// path := aih.assetsDir + "/" + newname
	// if err := ctx.SaveUploadedFile(file, path); err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// img := dto.AddArticleImage{
	// 	Src:  newname,
	// 	UUID: uuid,
	// }
	// aih.service.AddImage(img)
	// url := aih.assetsLink + "/" + newname
	// i := map[string]interface{}{
	// 	"url":  url,
	// 	"uuid": uuid,
	// }
	splitName := strings.Split(header.Filename, ".")
	ext := splitName[len(splitName)-1]
	newname := "img/" + uuid.String() + "." + ext
	path := pih.assetsDir + "/" + newname
	if err := ctx.SaveUploadedFile(header, path); err != nil {
		fmt.Println(err)
		return
	}
	img := dto.AddProgramImage{
		Src:  newname,
		UUID: uuid,
	}
	pih.service.AddImage(img)

	data := map[string]interface{}{
		"url":  pih.assetsLink + "/" + newname,
		"uuid": uuid,
		// "image_id":  image.UUID.String(),
	}
	res := helper.ResponseBuilder("Add Program Image Success", data)
	ctx.JSON(200, res)
}

func NewProgramImgHandler(service service.ProgramImgService, asstesDir string, asstesLink string) ProgramImgHandler {
	return &programImgHandler{
		service:    service,
		assetsDir:  asstesDir,
		assetsLink: asstesLink,
	}
}
