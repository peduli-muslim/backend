package handler

import (
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
)

type UploadImgHandler interface {
	Upload(ctx *gin.Context)
}

type uploadImgHandler struct {
	service service.UploadImgService
}

func (uh *uploadImgHandler) Upload(ctx *gin.Context) {
	file, header, err := ctx.Request.FormFile("file")
	if err != nil {
		res := helper.ErrorResponseBuilder("Form file required", nil)
		ctx.JSON(400, res)
		return
	}
	url, err := uh.service.Upload(file, header)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	data := map[string]string{
		"url": url,
	}
	res := helper.ResponseBuilder("Upload Image Success", data)
	ctx.JSON(200, res)
}

func NewUploadImgHandler(service service.UploadImgService) UploadImgHandler {
	return &uploadImgHandler{
		service: service,
	}
}
