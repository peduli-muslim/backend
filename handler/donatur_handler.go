package handler

import (
	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type DonaturHandler interface {
	CreateDonatur(ctx *gin.Context)
	VerifikasiPembayaran(ctx *gin.Context)
	GetAllbyProgram(ctx *gin.Context)
}

type donaturHandler struct {
	donaturService service.DonaturService
	userService    service.UserService
}

func (dh *donaturHandler) CreateDonatur(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			var donatur dto.CreateDonatur
			ctx.BindJSON(&donatur)
			if err := helper.Validate(&donatur); err != nil {
				errs := helper.ValidationError(err)
				res := helper.ErrorResponseBuilder("Validation error", errs)
				ctx.JSON(400, res)
				return
			}
			programID, err := uuid.Parse(ctx.Param("programID"))
			if err != nil {
				res := helper.ErrorResponseBuilder("Invalid programID", nil)
				ctx.JSON(400, res)
				return
			}
			if err = dh.donaturService.CreateDonatur(donatur, programID); err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			user := dto.UpdateUser{
				Nama:   donatur.Nama,
				NoTelp: donatur.NoTelp,
				Email:  donatur.Email,
			}
			dh.userService.UpdateUser(user, userID)
			res := helper.ResponseBuilder("Create new Dontur success", nil)
			ctx.JSON(200, res)
		}
	}
}

func (dh *donaturHandler) VerifikasiPembayaran(ctx *gin.Context) {
	var donatur dto.VerifyDonatur
	ctx.BindJSON(&donatur)
	if err := helper.Validate(&donatur); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := dh.donaturService.VerifikasiPembayaran(donatur); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Verifying Dontur success", nil)
	ctx.JSON(200, res)
}

func (dh *donaturHandler) GetAllbyProgram(ctx *gin.Context) {
	paramProgram := ctx.Param("programID")
	programID, err := uuid.Parse(paramProgram)
	if err != nil {
		res := helper.ErrorResponseBuilder("UUID format is wrong", nil)
		ctx.JSON(400, res)
		return
	}
	donaturs, err := dh.donaturService.GetAllbyProgram(programID)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get Public Donatur Success", donaturs)
	ctx.JSON(200, res)
}

func NewDonaturHandler(donaturService service.DonaturService, userService service.UserService) DonaturHandler {
	return &donaturHandler{
		donaturService: donaturService,
		userService:    userService,
	}
}
