package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type AuthHandler interface {
	UpdateUsernameUser(ctx *gin.Context)
	UpdatePasswordUser(ctx *gin.Context)
	UpdateUsernameAdmin(ctx *gin.Context)
	UpdatePasswordAdmin(ctx *gin.Context)
	GetAuth(ctx *gin.Context)
}

type authHandler struct {
	authService           service.AuthService
	userService           service.UserService
	jwtService            service.JwtSerivce
	blacklistTokenService service.BlacklistTokenService
}

func (ah *authHandler) UpdateUsernameUser(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			var username dto.UpdateUsername
			ctx.BindJSON(&username)
			if err := helper.Validate(&username); err != nil {
				errs := helper.ValidationError(err)
				res := helper.ErrorResponseBuilder("Validation error", errs)
				ctx.JSON(400, res)
				return
			}
			fmt.Println(userID)
			auth, err := ah.authService.GetAuth(userID)
			if err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			if auth.Username == username.Username || !ah.authService.CheckUser(username.Username) {
				if err := ah.authService.UpdateUsername(username, userID); err != nil {
					e := err.(*helper.CustomError)
					res := helper.ErrorResponseBuilder(e.Message, nil)
					ctx.JSON(e.Code, res)
					return
				}
				auth, _ := ah.authService.GetUser(userID)
				data := map[string]interface{}{
					"auth": auth,
				}
				res := helper.ResponseBuilder("Update Username success", data)
				ctx.JSON(200, res)
				return
			}
			res := helper.ErrorResponseBuilder("Update Username Failed", "Username exist. Please use another unsername")
			ctx.JSON(400, res)
			return
		}
		fmt.Println("A")
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	fmt.Println("B")
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *authHandler) UpdatePasswordUser(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			var password dto.UpdatePassword
			ctx.BindJSON(&password)
			if err := helper.Validate(&password); err != nil {
				errs := helper.ValidationError(err)
				fmt.Println(errs)
				res := helper.ErrorResponseBuilder("Validation error", errs)
				ctx.JSON(400, res)
				return
			}
			if err := ah.authService.UpdatePassword(password, userID); err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			auth, _ := ah.authService.GetUser(userID)
			data := map[string]interface{}{
				"auth": auth,
			}
			res := helper.ResponseBuilder("Update Password success", data)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *authHandler) UpdateUsernameAdmin(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		if adminID, ok := getAdminID.(uuid.UUID); ok {
			var username dto.UpdateUsername
			ctx.BindJSON(&username)
			if err := helper.Validate(&username); err != nil {
				errs := helper.ValidationError(err)
				res := helper.ErrorResponseBuilder("Validation error", errs)
				ctx.JSON(400, res)
				return
			}
			fmt.Println(adminID)
			auth, err := ah.authService.GetAuth(adminID)
			if err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			if auth.Username == username.Username || !ah.authService.CheckUser(username.Username) {
				if err := ah.authService.UpdateUsername(username, adminID); err != nil {
					e := err.(*helper.CustomError)
					res := helper.ErrorResponseBuilder(e.Message, nil)
					ctx.JSON(e.Code, res)
					return
				}
				auth, _ := ah.authService.GetAdmin(adminID)
				fmt.Println(auth)
				data := map[string]interface{}{
					"auth": auth,
				}
				res := helper.ResponseBuilder("Update Username success", data)
				ctx.JSON(200, res)
				return
			}
			res := helper.ErrorResponseBuilder("Update Username Failed", "Username exist. Please use another unsername")
			ctx.JSON(400, res)
			return
		}
		fmt.Println("A")
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	fmt.Println("B")
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *authHandler) UpdatePasswordAdmin(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		if adminID, ok := getAdminID.(uuid.UUID); ok {
			var password dto.UpdatePassword
			ctx.BindJSON(&password)
			if err := helper.Validate(&password); err != nil {
				errs := helper.ValidationError(err)
				fmt.Println(errs)
				res := helper.ErrorResponseBuilder("Validation error", errs)
				ctx.JSON(400, res)
				return
			}
			if err := ah.authService.UpdatePassword(password, adminID); err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			auth, _ := ah.authService.GetAdmin(adminID)
			data := map[string]interface{}{
				"auth": auth,
			}
			res := helper.ResponseBuilder("Update Password success", data)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *authHandler) GetAuth(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			auth, err := ah.authService.GetAuth(userID)
			if err != nil {
				e := err.(*helper.CustomError)
				res := helper.ErrorResponseBuilder(e.Message, nil)
				ctx.JSON(e.Code, res)
				return
			}
			data := map[string]interface{}{
				// "username": auth.Username,
				// "email":    auth.Email,
			}
			data["username"] = auth.Username
			res := helper.ResponseBuilder("Get Auth success", data)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *authHandler) Refresh(ctx *gin.Context) {
	if getUserID, ok := ctx.Get("userID"); ok {
		if userID, ok := getUserID.(uuid.UUID); ok {
			accessToken := ah.jwtService.GenerateToken(userID, "user", "access_token")
			res := helper.ResponseBuilder("Refresh token success", map[string]interface{}{
				"access_token": accessToken,
			})
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func NewAuthHandler(authService service.AuthService, userService service.UserService, jwtService service.JwtSerivce, blacklistTokenService service.BlacklistTokenService) AuthHandler {
	return &authHandler{
		authService:           authService,
		userService:           userService,
		jwtService:            jwtService,
		blacklistTokenService: blacklistTokenService,
	}
}
