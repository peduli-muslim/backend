package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ProgramHandler interface {
	CreateProgram(ctx *gin.Context)
	GetAllPrograms(ctx *gin.Context)
	GetOneProgram(ctx *gin.Context)
	UpdateProgram(ctx *gin.Context)
	ToggleStatus(ctx *gin.Context)
	DeleteProgram(ctx *gin.Context)
}

type programHandler struct {
	service service.ProgramService
}

func (ph *programHandler) CreateProgram(ctx *gin.Context) {
	var program dto.CreateProgram
	ctx.BindJSON(&program)
	fmt.Println(program)
	if err := helper.Validate(program); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ph.service.CreateProgram(program); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Sukses menambahkan Program baru", nil)
	ctx.JSON(200, res)
}

func (ph *programHandler) GetAllPrograms(ctx *gin.Context) {
	programs, err := ph.service.GetAllPrograms()
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get all Programs success", programs)
	ctx.JSON(200, res)
}

func (ph *programHandler) GetOneProgram(ctx *gin.Context) {
	paramIid := ctx.Param("id")
	uuid, err := uuid.Parse(paramIid)
	if err != nil {
		res := helper.ErrorResponseBuilder("UUID format is wrong", nil)
		ctx.JSON(400, res)
		return
	}
	program, err := ph.service.GetOneProgram(uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get Program success", program)
	ctx.JSON(200, res)
}

func (ph *programHandler) UpdateProgram(ctx *gin.Context) {
	paramIid := ctx.Param("id")
	uuid, err := uuid.Parse(paramIid)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid id", nil)
		ctx.JSON(400, res)
	}
	var program dto.UpdateProgram
	ctx.BindJSON(&program)
	if err := helper.Validate(program); err != nil {
		errs := helper.ValidationError(err)
		fmt.Println(errs)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	err = ph.service.UpdateProgram(program, uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Sukses mengupdate Program", nil)
	ctx.JSON(200, res)
}

func (ph *programHandler) ToggleStatus(ctx *gin.Context) {
	paramIid := ctx.Param("id")
	uuid, err := uuid.Parse(paramIid)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid id", nil)
		ctx.JSON(400, res)
	}
	err = ph.service.ToggleStatus(uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Sukses menutup Program", nil)
	ctx.JSON(200, res)
}

func (ph *programHandler) DeleteProgram(ctx *gin.Context) {
	paramIid := ctx.Param("id")
	uuid, err := uuid.Parse(paramIid)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid id", nil)
		ctx.JSON(400, res)
		return
	}
	err = ph.service.DeleteProgram(uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Sukses menghapus Program", nil)
	ctx.JSON(200, res)
}

func NewProgramHandler(service service.ProgramService) ProgramHandler {
	return &programHandler{
		service: service,
	}
}
