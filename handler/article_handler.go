package handler

import (
	"fmt"
	"strings"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type ArticleHandler interface {
	CreateArticle(ctx *gin.Context)
	GetOne(ctx *gin.Context)
	Update(ctx *gin.Context)
	GetAll(ctx *gin.Context)
}

type articleHandler struct {
	service    service.ArticleService
	imgService service.ArticleImgService
	assetsLink string
}

func (ah *articleHandler) CreateArticle(ctx *gin.Context) {
	var article dto.AddArticle
	ctx.BindJSON(&article)
	if err := helper.Validate(article); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	art, err := ah.service.CreateArticle(article)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	for _, img := range article.Gambar {
		imgId, err := uuid.Parse(img["uuid"])
		if err != nil {
			res := helper.ErrorResponseBuilder("Invalid Gambar Id", nil)
			ctx.JSON(400, res)
			return
		}
		// image := dto.AddArticleImage{
		// 	Src:  strings.ReplaceAll(img["url"], ah.assetsLink+"/", ""),
		// 	UUID: imgId,
		// }
		image := dto.UpdateArticleImage{
			Src:       strings.ReplaceAll(img["url"], ah.assetsLink+"/", ""),
			ArticleID: art.UUID,
		}
		// if _, err = ah.imgService.AddImage(image); err != nil {
		if err = ah.imgService.Update(image, imgId); err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
	}
	res := helper.ResponseBuilder("Sukses menambahkan Article baru", nil)
	ctx.JSON(200, res)
}

func (ah *articleHandler) GetOne(ctx *gin.Context) {
	paramIid := ctx.Param("uuid")
	uuid, err := uuid.Parse(paramIid)
	if err != nil {
		res := helper.ErrorResponseBuilder("UUID format is wrong", nil)
		ctx.JSON(400, res)
		return
	}
	article, err := ah.service.GetOne(uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	imgs := []map[string]string{}
	for _, img := range ah.imgService.GetByArticlelID(article.UUID) {
		imgs = append(imgs, map[string]string{
			"uuid": img.UUID.String(),
			"url":  img.Url,
		})
	}
	r := dto.ResArtikel{
		UUID:      article.UUID,
		Judul:     article.Judul,
		Kategori:  article.Kategori,
		Gambar:    imgs,
		Artikel:   article.Artikel,
		CreatedAt: article.CreatedAt,
	}
	res := helper.ResponseBuilder("Get Article success", r)
	ctx.JSON(200, res)
}

func (ah *articleHandler) Update(ctx *gin.Context) {
	paramIid := ctx.Param("uuid")
	id, err := uuid.Parse(paramIid)
	if err != nil {
		fmt.Println("A")
		res := helper.ErrorResponseBuilder("Invalid id", nil)
		ctx.JSON(400, res)
		return
	}
	var article dto.UpdateArticle
	ctx.BindJSON(&article)
	if err := helper.Validate(article); err != nil {
		fmt.Println("B")
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	err = ah.service.Update(article, id)
	if err != nil {
		fmt.Println("C")
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}

	for _, added := range article.AddedGambar {
		fmt.Println(added)
		imgId, err := uuid.Parse(added["uuid"])
		if err != nil {
			fmt.Println("D")
			res := helper.ErrorResponseBuilder("Invalid Gambar Id", nil)
			ctx.JSON(400, res)
			return
		}
		upImg := dto.UpdateArticleImage{
			Src:       added["url"],
			ArticleID: id,
		}
		if err = ah.imgService.Update(upImg, imgId); err != nil {
			fmt.Println("F")
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
	}
	for _, deleted := range article.DeletedGambar {
		imgId, err := uuid.Parse(deleted["uuid"])
		if err != nil {
			fmt.Println("G")
			res := helper.ErrorResponseBuilder("Invalid Gambar Id", nil)
			ctx.JSON(400, res)
			return
		}
		if err = ah.imgService.Delete(imgId); err != nil {
			fmt.Println("H")
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
	}
	res := helper.ResponseBuilder("Sukses mengupdate Article", nil)
	ctx.JSON(200, res)
}

func (ah *articleHandler) GetAll(ctx *gin.Context) {
	artikels, err := ah.service.GetAll()
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	resArtikels := []dto.ResArtikel{}
	for _, artikel := range artikels {
		var gambars []map[string]string
		for _, g := range ah.imgService.GetByArticlelID(artikel.UUID) {
			gambars = append(gambars, map[string]string{
				"uuid": g.UUID.String(),
				"url":  g.Url,
			})
		}
		resArtikels = append(resArtikels, dto.ResArtikel{
			UUID:      artikel.UUID,
			Judul:     artikel.Judul,
			Kategori:  artikel.Kategori,
			Gambar:    gambars,
			Artikel:   artikel.Artikel,
			CreatedAt: artikel.CreatedAt,
		})
	}
	res := helper.ResponseBuilder("Get all Articles success", resArtikels)
	ctx.JSON(200, res)
}

func NewArticleHandler(service service.ArticleService, imgService service.ArticleImgService, assetsLink string) ArticleHandler {
	return &articleHandler{
		service:    service,
		imgService: imgService,
		assetsLink: assetsLink,
	}
}
