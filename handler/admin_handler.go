package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

type AdminHandler interface {
	CreateAdmin(ctx *gin.Context)
	UpdateAdmin(ctx *gin.Context)
	UpdateAdminSelf(ctx *gin.Context)
	// UpdatePassword(ctx *gin.Context)
	Login(ctx *gin.Context)
	Logout(ctx *gin.Context)
	GetAll(ctx *gin.Context)
	GetOne(ctx *gin.Context)
	GetOneSelf(ctx *gin.Context)
	Refresh(ctx *gin.Context)
}

type adminHandler struct {
	adminService          service.AdminService
	authService           service.AuthService
	jwtService            service.JwtSerivce
	blacklistTokenService service.BlacklistTokenService
	emailService          service.EmailService
}

func (ah *adminHandler) CreateAdmin(ctx *gin.Context) {
	var admin dto.CreateAdmin
	ctx.BindJSON(&admin)
	if err := helper.Validate(&admin); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	adminCreate, err := ah.adminService.CreateAdmin(admin)
	if err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	_, err = ah.authService.CreateAuth(dto.Register{
		UserID: adminCreate.UUID,
		// Email:  admin.Email,
	})
	if err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	emailBody := fmt.Sprintf("Wellcome %s", adminCreate.Nama)
	if err := ah.emailService.Send(admin.Email, "Registration", emailBody); err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Create new Admin success", nil)
	ctx.JSON(200, res)
}

func (ah *adminHandler) UpdateAdmin(ctx *gin.Context) {
	if uuidParam, ok := ctx.Params.Get("uuid"); ok {
		uuid, err := uuid.Parse(uuidParam)
		if err != nil {
			res := helper.ErrorResponseBuilder("Invalid uuid", nil)
			ctx.JSON(400, res)
			return
		}
		var admin dto.UpdateAdmin
		ctx.BindJSON(&admin)
		if err := helper.Validate(&admin); err != nil {
			errs := helper.ValidationError(err)
			res := helper.ErrorResponseBuilder("Validation error", errs)
			ctx.JSON(400, res)
			return
		}
		if err := ah.adminService.UpdateAdmin(admin, uuid); err != nil {
			e, _ := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		res := helper.ResponseBuilder("Update Admin success", nil)
		ctx.JSON(200, res)
	}
}

func (ah *adminHandler) UpdateAdminSelf(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		adminID := getAdminID.(uuid.UUID)
		var admin dto.UpdateAdminSelf
		ctx.BindJSON(&admin)
		if err := helper.Validate(&admin); err != nil {
			errs := helper.ValidationError(err)
			res := helper.ErrorResponseBuilder("Validation error", errs)
			ctx.JSON(400, res)
			return
		}
		if err := ah.adminService.UpdateAdminSelf(admin, adminID); err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		profile, _ := ah.authService.GetAdmin(adminID)
		res := helper.ResponseBuilder("Update Admin success", profile)
		ctx.JSON(200, res)
	}
}

func (ah *adminHandler) Login(ctx *gin.Context) {
	var login dto.Login
	ctx.BindJSON(&login)
	if err := helper.Validate(&login); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	admin, err := ah.authService.LoginAdmin(login)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, nil)
		ctx.JSON(e.Code, res)
		return
	}
	data := map[string]interface{}{
		"admin": admin,
	}
	data["access_token"] = ah.jwtService.GenerateToken(admin.UserID, "admin", "access_token")
	data["refresh_token"] = nil
	if login.RememberMe {
		data["refresh_token"] = ah.jwtService.GenerateToken(admin.UserID, "admin", "refresh_token")
	}
	res := helper.ResponseBuilder("Login success", data)
	ctx.JSON(200, res)

}

func (ah *adminHandler) Logout(ctx *gin.Context) {
	if getTokenClaim, ok := ctx.Get("tokenClaim"); ok {
		if tokenClaim, ok := getTokenClaim.(jwt.MapClaims); ok {
			ah.blacklistTokenService.CreateBlacklistToken(tokenClaim["jti"].(string))
			res := helper.ResponseBuilder("Logout success", nil)
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *adminHandler) Refresh(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		if adminID, ok := getAdminID.(uuid.UUID); ok {
			accessToken := ah.jwtService.GenerateToken(adminID, "admin", "access_token")
			res := helper.ResponseBuilder("Refresh token success", map[string]interface{}{
				"access_token": accessToken,
			})
			ctx.JSON(200, res)
			return
		}
		msg := map[string]string{"msg": "Invalid token"}
		res := helper.ErrorResponseBuilder("Unauthorized", msg)
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *adminHandler) GetAll(ctx *gin.Context) {
	admins, err := ah.adminService.GetAll()
	if err != nil {
		res := helper.ErrorResponseBuilder("Unprocessable entity", nil)
		ctx.JSON(422, res)
		return
	}
	res := helper.ResponseBuilder("Get All Admin Success", admins)
	ctx.JSON(200, res)
}

func (ah *adminHandler) GetOne(ctx *gin.Context) {
	if uuidParam, ok := ctx.Params.Get("uuid"); ok {
		uuid, err := uuid.Parse(uuidParam)
		if err != nil {
			res := helper.ErrorResponseBuilder("Invalid uuid", nil)
			ctx.JSON(400, res)
			return
		}
		admin, err := ah.adminService.GetOne(uuid)
		if err != nil {
			res := helper.ErrorResponseBuilder("Unprocessable entity", nil)
			ctx.JSON(422, res)
			return
		}
		res := helper.ResponseBuilder("Get One Admin Success", admin)
		ctx.JSON(200, res)
	}
}

func (ah *adminHandler) GetOneSelf(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		adminID := getAdminID.(uuid.UUID)
		admin, err := ah.adminService.GetOne(adminID)
		if err != nil {
			res := helper.ErrorResponseBuilder("Unprocessable entity", nil)
			ctx.JSON(422, res)
			return
		}
		res := helper.ResponseBuilder("Get One Admin Success", admin)
		ctx.JSON(200, res)
	}
}

func NewAdminHandler(adminService service.AdminService, authService service.AuthService, jwtService service.JwtSerivce, blacklistTokenService service.BlacklistTokenService, emailService service.EmailService) AdminHandler {
	return &adminHandler{
		adminService:          adminService,
		authService:           authService,
		jwtService:            jwtService,
		blacklistTokenService: blacklistTokenService,
		emailService:          emailService,
	}
}
