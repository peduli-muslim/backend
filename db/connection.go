package db

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/config"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func InitializeDB(config *config.Config) *gorm.DB {
	var dialect gorm.Dialector
	if config.Dialect == "mysql" {
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.DB_User, config.DB_Pass, config.DB_Host, config.DB_Port, config.DB_Name)
		dialect = mysql.Open(dsn)
	} else {
		dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Makassar", config.DB_Host, config.DB_User, config.DB_Pass, config.DB_Name, config.DB_Port)
		fmt.Println(dsn)
		dialect = postgres.Open(dsn)
	}
	// time.Sleep(30 * time.Second)
	connection, err := gorm.Open(dialect, &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		panic("DB connection failed")
	} else {
		fmt.Println("DB connction success")
	}
	errMigrate := connection.AutoMigrate(
		entity.Program{},
		entity.Donatur{},
		entity.User{},
		entity.Auth{},
		entity.Admin{},
		entity.ProgramImage{},
		entity.Article{},
		entity.ArticleImage{},
		entity.BlacklistToken{},
	)
	if errMigrate != nil {
		panic(errMigrate)
	}
	return connection
}

func CloseDB(connction *gorm.DB) {
	db, err := connction.DB()
	if err != nil {
		panic("Failed to close DB connection")
	}
	db.Close()
}
