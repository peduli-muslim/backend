package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ArticleRepository interface {
	CreateArticle(article *entity.Article) *gorm.DB
	GetAll() ([]entity.Article, *gorm.DB)
	GetAllKategori(kategori []string) ([]entity.Article, *gorm.DB)
	GetOne(uuid uuid.UUID) (entity.Article, *gorm.DB)
	Update(article entity.Article, uuid uuid.UUID) *gorm.DB
}

type articleRepository struct {
	connection *gorm.DB
}

func (pr *articleRepository) CreateArticle(article *entity.Article) *gorm.DB {
	dbRes := pr.connection.Create(article)
	return dbRes
}

func (pr *articleRepository) GetAll() ([]entity.Article, *gorm.DB) {
	var articles []entity.Article
	dbRes := pr.connection.Order("created_at").Find(&articles)
	return articles, dbRes
}

func (pr *articleRepository) GetAllKategori(kategori []string) ([]entity.Article, *gorm.DB) {
	var articles []entity.Article
	// dbRes := pr.connection.Model(entity.Article{}).Where("kategori like %?%", kategori).Order("created_at").Find(&articles)
	dbRes := pr.connection.Model(entity.Article{})
	for _, k := range kategori {
		dbRes = dbRes.Where("kategori like %?%", k)
	}
	dbRes.Order("created_at").Find(&articles)
	return articles, dbRes
}

func (pr *articleRepository) GetOne(uuid uuid.UUID) (entity.Article, *gorm.DB) {
	var article entity.Article
	dbRes := pr.connection.Where("uuid = ?", uuid).First(&article)
	return article, dbRes
}

func (pr *articleRepository) Update(article entity.Article, uuid uuid.UUID) *gorm.DB {
	dbRes := pr.connection.Model(entity.Article{}).Where("uuid = ?", uuid).Updates(article)
	return dbRes
}

func NewArticleRepository(connection *gorm.DB) ArticleRepository {
	return &articleRepository{
		connection: connection,
	}
}
