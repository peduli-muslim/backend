package repository

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ProgramImgRepository interface {
	AddImage(image *entity.ProgramImage) *gorm.DB
	GetOne(programID uuid.UUID) (entity.ProgramImage, *gorm.DB)
	Update(programImg *entity.ProgramImage, filename string) *gorm.DB
}

type programImgRepository struct {
	connection *gorm.DB
}

func (pir *programImgRepository) AddImage(image *entity.ProgramImage) *gorm.DB {
	dbRes := pir.connection.Create(image)
	return dbRes
}

func (pir *programImgRepository) GetOne(programID uuid.UUID) (entity.ProgramImage, *gorm.DB) {
	var image entity.ProgramImage
	dbRes := pir.connection.Where("uuid = ?", programID).Find(&image)
	return image, dbRes
}

func (pir *programImgRepository) Update(programImg *entity.ProgramImage, filename string) *gorm.DB {
	fmt.Println("Repository")
	fmt.Println(programImg)
	dbRes := pir.connection.Model(entity.ProgramImage{}).Where("src = ?", filename).Updates(programImg)
	return dbRes
}

func NewProgramImgRepository(connection *gorm.DB) ProgramImgRepository {
	return &programImgRepository{
		connection: connection,
	}
}
