package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	UserID      uuid.UUID `gorm:"type:char(36);not null" json:"userID"`
	Username    string    `gorm:"size:50" json:"username"`
	Password    string    `gorm:"size:255" json:"-"`
	PasswordSet bool      `gorm:"password_set" json:"password_set"`
	Email       string    `gorm:"size:150" json:"email"`
	GID         string    `gorm:"size:30" json:"-"`
	Nama        string    `gorm:"size:150" json:"nama"`
	NoTelp      string    `gorm:"size:20" json:"no_telp"`
}

type Admin struct {
	UserID      uuid.UUID `gorm:"type:char(36);not null" json:"userID"`
	Username    string    `gorm:"size:50" json:"username"`
	Password    string    `gorm:"size:255" json:"-"`
	PasswordSet bool      `gorm:"password_set" json:"password_set"`
	Email       string    `gorm:"size:150" json:"email"`
	GID         string    `gorm:"size:30" json:"-"`
	Nama        string    `gorm:"size:150" json:"nama"`
	Super       bool      `gorm:"bool;default:false" json:"super"`
}

type AuthRepository interface {
	CreateAuth(auth *entity.Auth) *gorm.DB
	UpdateAuth(auth *entity.Auth, userID uuid.UUID) *gorm.DB
	GetByUsername(username string) (entity.Auth, *gorm.DB)
	GetByUserID(userID uuid.UUID) (entity.Auth, *gorm.DB)
	GetUser(userID uuid.UUID) (User, *gorm.DB)
	GetAdmin(adminID uuid.UUID) (Admin, *gorm.DB)
	GetUserByEmail(email string) (User, *gorm.DB)
	GetAdminByEmail(email string) (Admin, *gorm.DB)
	GetUserByUsername(username string) (User, *gorm.DB)
	GetAdminByUsername(username string) (Admin, *gorm.DB)
	GetUserByGID(gid string) (User, *gorm.DB)
	GetAdminByGID(gid string) (Admin, *gorm.DB)
}

type authRepository struct {
	connection *gorm.DB
}

func (ar *authRepository) CreateAuth(auth *entity.Auth) *gorm.DB {
	dbRes := ar.connection.Create(auth)
	return dbRes
}

func (ur *authRepository) UpdateAuth(auth *entity.Auth, userID uuid.UUID) *gorm.DB {
	dbRes := ur.connection.Model(entity.Auth{}).Where("user_id = ?", userID).Updates(auth)
	return dbRes
}

func (ur *authRepository) GetByUsername(username string) (entity.Auth, *gorm.DB) {
	var auth entity.Auth
	dbRes := ur.connection.Where("username = ?", username).First(&auth)
	return auth, dbRes
}

func (ur *authRepository) GetByUserID(userID uuid.UUID) (entity.Auth, *gorm.DB) {
	var auth entity.Auth
	dbRes := ur.connection.Where("user_id = ?", userID).First(&auth)
	return auth, dbRes
}

func (ur *authRepository) GetUser(userID uuid.UUID) (User, *gorm.DB) {
	var user User
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, users.email, users.g_id, users.nama, users.no_telp").Joins("left join users on auths.user_id = users.uuid").Where("users.uuid = ?", userID).First(&user)
	return user, dbRes
}

func (ur *authRepository) GetAdmin(adminID uuid.UUID) (Admin, *gorm.DB) {
	var admin Admin
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, admins.email, admins.g_id, admins.nama, admins.super").Joins("left join admins on auths.user_id = admins.uuid").Where("admins.uuid = ?", adminID).First(&admin)
	return admin, dbRes
}

func (ur *authRepository) GetUserByEmail(email string) (User, *gorm.DB) {
	var user User
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, users.email, users.g_id, users.nama, users.no_telp").Joins("left join users on auths.user_id = users.uuid").Where("users.email = ?", email).First(&user)
	return user, dbRes
}

func (ur *authRepository) GetAdminByEmail(email string) (Admin, *gorm.DB) {
	var admin Admin
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, admins.email, admins.g_id, admins.nama, admins.super").Joins("left join admins on auths.user_id = admins.uuid").Where("admins.email = ?", email).First(&admin)
	return admin, dbRes
}

func (ur *authRepository) GetUserByUsername(username string) (User, *gorm.DB) {
	var user User
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, users.email, users.g_id, users.nama, users.no_telp").Joins("left join users on auths.user_id = users.uuid").Where("auths.username = ?", username).First(&user)
	return user, dbRes
}

func (ur *authRepository) GetAdminByUsername(username string) (Admin, *gorm.DB) {
	var admin Admin
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, admins.email, admins.g_id, admins.nama, admins.super").Joins("left join admins on auths.user_id = admins.uuid").Where("auths.username = ?", username).First(&admin)
	return admin, dbRes
}

func (ur *authRepository) GetUserByGID(gid string) (User, *gorm.DB) {
	var user User
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, users.email, users.g_id, users.nama, users.no_telp").Joins("left join users on auths.user_id = users.uuid").Where("users.g_id = ?", gid).First(&user)
	return user, dbRes
}

func (ur *authRepository) GetAdminByGID(gid string) (Admin, *gorm.DB) {
	var admin Admin
	dbRes := ur.connection.Model(&entity.Auth{}).Select("auths.user_id, auths.username, auths.password, auths.password != '' password_set, admins.email, admins.g_id, admins.nama, admins.super").Joins("left join admins on auths.user_id = admins.uuid").Where("admins.g_id = ?", gid).First(&admin)
	return admin, dbRes
}

func NewAuthRepository(connection *gorm.DB) AuthRepository {
	return &authRepository{
		connection: connection,
	}
}
