package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type AdminRepository interface {
	CreateAdmin(admin *entity.Admin) *gorm.DB
	GetOne(uuid uuid.UUID) (entity.Admin, *gorm.DB)
	GetByUsername(adminname string) (entity.Admin, *gorm.DB)
	GetByEmail(email string) (entity.Admin, *gorm.DB)
	UpdateAdmin(admin *entity.Admin, uuid uuid.UUID) *gorm.DB
	GetAll() ([]entity.Admin, *gorm.DB)
	GetOnebyGID(gid string) (entity.Admin, *gorm.DB)
}

type adminRepository struct {
	connection *gorm.DB
}

func (ar *adminRepository) CreateAdmin(admin *entity.Admin) *gorm.DB {
	dbRes := ar.connection.Create(admin)
	return dbRes
}

func (ar *adminRepository) GetOne(uuid uuid.UUID) (entity.Admin, *gorm.DB) {
	var admin entity.Admin
	// ar.connection.Table("admins")
	dbRes := ar.connection.Where("uuid = ?", uuid).First(&admin)
	return admin, dbRes
}

func (ar *adminRepository) GetByUsername(adminname string) (entity.Admin, *gorm.DB) {
	var admin entity.Admin
	dbRes := ar.connection.Where("username = ?", adminname).First(&admin)
	return admin, dbRes
}

func (ar *adminRepository) GetByEmail(email string) (entity.Admin, *gorm.DB) {
	var admin entity.Admin
	dbRes := ar.connection.Where("email = ?", email).First(&admin)
	return admin, dbRes
}

func (ar *adminRepository) UpdateAdmin(admin *entity.Admin, uuid uuid.UUID) *gorm.DB {
	dbRes := ar.connection.Model(entity.Admin{}).Where("uuid = ?", uuid).Updates(admin)
	return dbRes
}

func (ar *adminRepository) GetAll() ([]entity.Admin, *gorm.DB) {
	var admins []entity.Admin
	dbRes := ar.connection.Find(&admins)
	return admins, dbRes
}

func (ar *adminRepository) GetOnebyGID(gid string) (entity.Admin, *gorm.DB) {
	var admin entity.Admin
	dbRes := ar.connection.Where("g_id = ?", gid).First(&admin)
	return admin, dbRes
}

func NewAdminRepository(connection *gorm.DB) AdminRepository {
	return &adminRepository{
		connection: connection,
	}
}
