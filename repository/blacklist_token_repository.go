package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"gorm.io/gorm"
)

type BlacklistTokenRepository interface {
	CreateBlacklistToken(blacklistToken *entity.BlacklistToken) *gorm.DB
	// return true if token has been blacklisted false other wise
	CheckBlacklistToken(jti string) bool
}

type blacklistTokenRepository struct {
	connection *gorm.DB
}

func (ar *blacklistTokenRepository) CreateBlacklistToken(blacklistToken *entity.BlacklistToken) *gorm.DB {
	dbRes := ar.connection.Create(blacklistToken)
	return dbRes
}

func (ur *blacklistTokenRepository) CheckBlacklistToken(jti string) bool {
	var blacklists []entity.BlacklistToken
	ur.connection.Where("jti = ?", jti).Find(&blacklists)
	return len(blacklists) != 0
}

func NewBlacklistTokenRepository(connection *gorm.DB) BlacklistTokenRepository {
	return &blacklistTokenRepository{
		connection: connection,
	}
}
