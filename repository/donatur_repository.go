package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type DonaturRepository interface {
	CreateDonatur(donatur *entity.Donatur) *gorm.DB
	GetOneDonatur(uuid uuid.UUID) (entity.Donatur, *gorm.DB)
	UpdateDonatur(donatur entity.Donatur, uuid uuid.UUID) *gorm.DB
	GetAllbyProgram(programID uuid.UUID) ([]entity.Donatur, *gorm.DB)
}

type donaturRepository struct {
	connection *gorm.DB
}

func (dr *donaturRepository) CreateDonatur(donatur *entity.Donatur) *gorm.DB {
	dbRes := dr.connection.Create(donatur)
	return dbRes
}

func (dr *donaturRepository) GetOneDonatur(uuid uuid.UUID) (entity.Donatur, *gorm.DB) {
	var donatur entity.Donatur
	dbRes := dr.connection.Where("uuid = ?", uuid).First(&donatur)
	return donatur, dbRes
}

func (dr *donaturRepository) UpdateDonatur(donatur entity.Donatur, uuid uuid.UUID) *gorm.DB {
	dbRes := dr.connection.Model(entity.Donatur{}).Where("uuid = ?", uuid).Updates(donatur)
	return dbRes
}

func (dr *donaturRepository) GetAllbyProgram(programID uuid.UUID) ([]entity.Donatur, *gorm.DB) {
	var donaturs []entity.Donatur
	dbRes := dr.connection.Where("program_id = ? and verified", programID).Find(&donaturs)
	return donaturs, dbRes
}

func NewDonaturRepository(connection *gorm.DB) DonaturRepository {
	return &donaturRepository{
		connection: connection,
	}
}
