package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ArticleImgRepository interface {
	AddImage(image *entity.ArticleImage) *gorm.DB
	GetByArticlelID(articleID uuid.UUID) ([]entity.ArticleImage, *gorm.DB)
	Update(articleImg *entity.ArticleImage, uuid uuid.UUID) *gorm.DB
	Delete(uuid uuid.UUID) *gorm.DB
}

type articleImgRepository struct {
	connection *gorm.DB
}

func (air *articleImgRepository) AddImage(image *entity.ArticleImage) *gorm.DB {
	dbRes := air.connection.Create(image)
	return dbRes
}

func (air *articleImgRepository) GetByArticlelID(articleID uuid.UUID) ([]entity.ArticleImage, *gorm.DB) {
	var image []entity.ArticleImage
	dbRes := air.connection.Where("article_id = ?", articleID).Find(&image)
	return image, dbRes
}

func (air *articleImgRepository) Update(articleImg *entity.ArticleImage, uuid uuid.UUID) *gorm.DB {
	dbRes := air.connection.Model(entity.ArticleImage{}).Where("uuid = ?", uuid).Updates(articleImg)
	return dbRes
}

func (air *articleImgRepository) Delete(uuid uuid.UUID) *gorm.DB {
	dbRes := air.connection.Where("uuid = ?", uuid).Delete(entity.ArticleImage{})
	return dbRes
}

func NewArticleImgRepository(connection *gorm.DB) ArticleImgRepository {
	return &articleImgRepository{
		connection: connection,
	}
}
