package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type UserRepository interface {
	CreateUser(user *entity.User) *gorm.DB
	GetOneUser(uuid uuid.UUID) (entity.User, *gorm.DB)
	GetOnebyGID(gid string) (entity.User, *gorm.DB)
	UpdateUser(user entity.User, uuid uuid.UUID) *gorm.DB
}

type userRepository struct {
	connection *gorm.DB
}

func (ur *userRepository) CreateUser(user *entity.User) *gorm.DB {
	dbRes := ur.connection.Create(user)
	return dbRes
}

func (ur *userRepository) GetOneUser(uuid uuid.UUID) (entity.User, *gorm.DB) {
	var user entity.User
	dbRes := ur.connection.Where("uuid = ?", uuid).First(&user)
	return user, dbRes
}

func (ur *userRepository) GetOnebyGID(gid string) (entity.User, *gorm.DB) {
	var user entity.User
	dbRes := ur.connection.Where("g_id = ?", gid).First(&user)
	return user, dbRes
}

func (ur *userRepository) UpdateUser(user entity.User, uuid uuid.UUID) *gorm.DB {
	dbRes := ur.connection.Model(entity.User{}).Where("uuid = ?", uuid).Updates(user)
	return dbRes
}

func NewUserRepository(connection *gorm.DB) UserRepository {
	return &userRepository{
		connection: connection,
	}
}
