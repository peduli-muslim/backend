package repository

import (
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ProgramRepository interface {
	CreateProgram(program *entity.Program) *gorm.DB
	GetAllPrograms() ([]entity.Program, *gorm.DB)
	GetOneProgram(uuid uuid.UUID) (entity.Program, *gorm.DB)
	UpdateProgram(program *entity.Program, uuid uuid.UUID) *gorm.DB
	DeleteProgram(uuid uuid.UUID) *gorm.DB
}

type programRepository struct {
	connection *gorm.DB
}

func (pr *programRepository) CreateProgram(program *entity.Program) *gorm.DB {
	dbRes := pr.connection.Create(program)
	return dbRes
}

func (pr *programRepository) GetAllPrograms() ([]entity.Program, *gorm.DB) {
	var programs []entity.Program
	dbRes := pr.connection.Order("created_at").Find(&programs)
	return programs, dbRes
}

func (pr *programRepository) GetOneProgram(uuid uuid.UUID) (entity.Program, *gorm.DB) {
	var program entity.Program
	dbRes := pr.connection.Where("uuid = ?", uuid).First(&program)
	return program, dbRes
}

func (pr *programRepository) UpdateProgram(program *entity.Program, uuid uuid.UUID) *gorm.DB {
	dbRes := pr.connection.Model(entity.Program{}).Where("uuid = ?", uuid).Updates(program)
	return dbRes
}

func (pr *programRepository) DeleteProgram(uuid uuid.UUID) *gorm.DB {
	dbRes := pr.connection.Where("uuid = ?", uuid).Delete(entity.Program{})
	return dbRes
}

func NewProgramRepository(connection *gorm.DB) ProgramRepository {
	return &programRepository{
		connection: connection,
	}
}
