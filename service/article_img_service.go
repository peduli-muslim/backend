package service

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type ArticleImgService interface {
	AddImage(image dto.AddArticleImage) (entity.ArticleImage, error)
	GetByArticlelID(uuid uuid.UUID) []entity.ArticleImage
	Update(articleImg dto.UpdateArticleImage, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type articleImgService struct {
	repository repository.ArticleImgRepository
}

func (ais *articleImgService) AddImage(image dto.AddArticleImage) (entity.ArticleImage, error) {
	addImage := entity.ArticleImage{
		UUID: image.UUID,
		Src:  image.Src,
	}
	dbRes := ais.repository.AddImage(&addImage)
	return addImage, dbRes.Error
}

func (ais *articleImgService) GetByArticlelID(uuid uuid.UUID) []entity.ArticleImage {
	articleImg, _ := ais.repository.GetByArticlelID(uuid)
	return articleImg
}

// func (ais *articleImgService) GetByArticlelID(postID uuid.UUID) (entity.ArticleImage, error) {
// 	articleImg, dbRes := ais.repository.GetByArticlelID(postID)
// 	return articleImg, dbRes.Error
// }

func (ais *articleImgService) Update(articleImg dto.UpdateArticleImage, uuid uuid.UUID) error {
	fmt.Println("Update")
	updateArticleImg := entity.ArticleImage{
		ArticleID: articleImg.ArticleID,
	}
	dbRes := ais.repository.Update(&updateArticleImg, uuid)
	return dbRes.Error
}

func (ais *articleImgService) Delete(uuid uuid.UUID) error {
	dbRes := ais.repository.Delete(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Delete Article Image", nil)
		return customErr
	}
	return nil
}

func NewArticleImgService(repository repository.ArticleImgRepository) ArticleImgService {
	return &articleImgService{
		repository: repository,
	}
}
