package service

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type UserService interface {
	CreateUser(user dto.CreateUser) (entity.User, error)
	GetOneUser(uuid uuid.UUID) (entity.User, error)
	GetOnebyGID(gid string) (entity.User, error)
	UpdateUser(user dto.UpdateUser, uuid uuid.UUID) (entity.User, error)
}

type userService struct {
	repository repository.UserRepository
}

func (us *userService) CreateUser(user dto.CreateUser) (entity.User, error) {
	userCreate := entity.User{
		GID:    user.GID,
		Nama:   user.Nama,
		NoTelp: user.NoTelp,
		Email:  user.Email,
	}
	dbRes := us.repository.CreateUser(&userCreate)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to create User", nil)
		return userCreate, customErr
	}
	return userCreate, nil
}

func (us *userService) GetOneUser(uuid uuid.UUID) (entity.User, error) {
	user, dbRes := us.repository.GetOneUser(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "User not found", nil)
		return user, customErr
	}
	return user, nil
}

func (us *userService) GetOnebyGID(gid string) (entity.User, error) {
	user, dbRes := us.repository.GetOnebyGID(gid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "User not found", nil)
		return user, customErr
	}
	return user, nil
}

func (us *userService) UpdateUser(user dto.UpdateUser, uuid uuid.UUID) (entity.User, error) {
	userUpdate := entity.User{
		Nama:   user.Nama,
		NoTelp: user.NoTelp,
		Email:  user.Email,
	}
	fmt.Println(userUpdate)
	dbRes := us.repository.UpdateUser(userUpdate, uuid)
	fmt.Println(dbRes.RowsAffected)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to update User", nil)
		return entity.User{}, customErr
	}
	return us.GetOneUser(uuid)
}

func NewUserService(repository repository.UserRepository) UserService {
	return &userService{
		repository: repository,
	}
}
