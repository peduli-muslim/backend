package service

import (
	"fmt"
	"time"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type ProgramService interface {
	CreateProgram(program dto.CreateProgram) error
	GetAllPrograms() ([]entity.Program, error)
	GetOneProgram(uuid uuid.UUID) (entity.Program, error)
	UpdateProgram(program dto.UpdateProgram, uuid uuid.UUID) error
	ToggleStatus(uuid uuid.UUID) error
	DeleteProgram(uuid uuid.UUID) error
}

type programService struct {
	programRepository repository.ProgramRepository
	imageRepository   repository.ProgramImgRepository
}

func (ps *programService) CreateProgram(program dto.CreateProgram) error {
	bukaSampai, err := time.Parse("2006-01-02", program.BukaSampai)
	if err != nil {
		customErr := helper.NewError(422, "Failed to parse time", nil)
		return customErr
	}
	if err != nil {
		customErr := helper.NewError(422, "Invalid donatur id", nil)
		return customErr
	}
	programCreate := entity.Program{
		Nama:       program.Nama,
		Kategori:   program.Kategori,
		Target:     program.Target,
		BukaSampai: bukaSampai,
		Image:      program.Image,
		Rangkuman:  program.Rangkuman,
	}
	dbRes := ps.programRepository.CreateProgram(&programCreate)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Create Program", nil)
		return customErr
	}
	fmt.Println(programCreate.UUID)
	image := entity.ProgramImage{
		ProgramID: programCreate.UUID,
	}
	fmt.Println(image)
	dbRes = ps.imageRepository.Update(&image, program.Image)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Create Program", nil)
		return customErr
	}
	return nil
}

func (ps *programService) GetAllPrograms() ([]entity.Program, error) {
	programs, dbRes := ps.programRepository.GetAllPrograms()
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Get All Programs", nil)
		return programs, customErr
	}
	return programs, nil
}

func (ps *programService) GetOneProgram(uuid uuid.UUID) (entity.Program, error) {
	program, dbRes := ps.programRepository.GetOneProgram(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Program not found", nil)
		return program, customErr
	}
	return program, dbRes.Error
}

func (ps *programService) UpdateProgram(program dto.UpdateProgram, uuid uuid.UUID) error {
	bukaSampai, err := time.Parse("2006-01-02", program.BukaSampai)
	if err != nil {
		customErr := helper.NewError(422, "Failed to parse time", nil)
		return customErr
	}
	if err != nil {
		customErr := helper.NewError(422, "Invalid donatur id", nil)
		return customErr
	}
	programUpdate := entity.Program{
		Nama:       program.Nama,
		Kategori:   program.Kategori,
		Target:     program.Target,
		Status:     program.Status,
		BukaSampai: bukaSampai,
		Rangkuman:  program.Rangkuman,
	}
	fmt.Println(programUpdate.Rangkuman)
	dbRes := ps.programRepository.UpdateProgram(&programUpdate, uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Update Program", nil)
		return customErr
	}
	return nil
}

func (ps *programService) ToggleStatus(uuid uuid.UUID) error {
	program, err := ps.GetOneProgram(uuid)
	if err != nil {
		return err
	}
	status := ""
	switch program.Status {
	case "Open":
		status = "Closed"
	case "Closed":
		status = "Open"
	}
	programUpdate := entity.Program{
		Status: status,
	}
	dbRes := ps.programRepository.UpdateProgram(&programUpdate, uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Close Program", nil)
		return customErr
	}
	return nil
}

func (ps *programService) DeleteProgram(uuid uuid.UUID) error {
	dbRes := ps.programRepository.DeleteProgram(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Delete Program", nil)
		return customErr
	}
	return dbRes.Error
}

func NewProgramService(programRepository repository.ProgramRepository, imageRepository repository.ProgramImgRepository) ProgramService {
	return &programService{
		programRepository: programRepository,
		imageRepository:   imageRepository,
	}
}
