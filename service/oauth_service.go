package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
)

type OAuthService interface {
	GetToken(ctx *gin.Context, code string) (*oauth2.Token, error)
	GetUserInfo(accessToken string) (dto.GoogleUserInfo, error)
	GetTokenInfo(accessToken string) (dto.GoogleTokenInfo, error)
}

type oAuthService struct {
	config oauth2.Config
}

func (oas *oAuthService) GetUserInfo(accessToken string) (dto.GoogleUserInfo, error) {
	var userInfo dto.GoogleUserInfo
	res, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken)
	if err != nil {
		fmt.Println("A")
		customErr := helper.NewError(422, "Failed to get UserInfo", nil)
		fmt.Println(customErr)
		return userInfo, customErr
	}
	defer res.Body.Close()
	r, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("B")
		customErr := helper.NewError(422, "Failed to read Body", nil)
		fmt.Println(customErr)
		return userInfo, customErr
	}
	err = json.Unmarshal(r, &userInfo)
	if err != nil {
		fmt.Println("C")
		customErr := helper.NewError(422, "Failed to unmarshal UserInfo", nil)
		fmt.Println(customErr)
		return userInfo, customErr
	}
	return userInfo, nil
}

func (oas *oAuthService) GetTokenInfo(accessToken string) (dto.GoogleTokenInfo, error) {
	var tokenInfo dto.GoogleTokenInfo
	res, err := http.Get("https://www.googleapis.com/oauth2/v2/tokeninfo?access_token=" + accessToken)
	if err != nil {
		fmt.Println("A")
		helper.NewError(422, "Failed to Get Token Info", nil)
		return tokenInfo, err
	}
	defer res.Body.Close()
	r, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("B")
		helper.NewError(422, "Failed to Get Token Info", nil)
		return tokenInfo, err
	}
	err = json.Unmarshal(r, &tokenInfo)
	if err != nil {
		fmt.Println("C")
		helper.NewError(422, "Failed to Get Token Info", nil)
		return tokenInfo, err
	}
	return tokenInfo, err
}

func (oas *oAuthService) GetToken(ctx *gin.Context, code string) (*oauth2.Token, error) {
	token, err := oas.config.Exchange(ctx, code)
	if err != nil {
		customErr := helper.NewError(400, "Invalid code", nil)
		fmt.Println(customErr)
		return token, customErr
	}
	return token, nil
}

func NewOAuthService(config oauth2.Config) OAuthService {
	return &oAuthService{
		config: config,
	}
}
