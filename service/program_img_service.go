package service

import (
	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type ProgramImgService interface {
	AddImage(image dto.AddProgramImage) (entity.ProgramImage, error)
	GetOne(programID uuid.UUID) (entity.ProgramImage, error)
	Udate(programImg dto.UpdateProgramImage, filename string) error
}

type programImgService struct {
	repository repository.ProgramImgRepository
}

func (pis *programImgService) AddImage(image dto.AddProgramImage) (entity.ProgramImage, error) {
	addImage := entity.ProgramImage{
		UUID: image.UUID,
		Src:  image.Src,
	}
	dbRes := pis.repository.AddImage(&addImage)
	return addImage, dbRes.Error
}

func (pis *programImgService) GetOne(programID uuid.UUID) (entity.ProgramImage, error) {
	programImg, dbRes := pis.repository.GetOne(programID)
	return programImg, dbRes.Error
}

func (pis *programImgService) Udate(programImg dto.UpdateProgramImage, filename string) error {
	updateProgramImg := entity.ProgramImage{
		ProgramID: programImg.ProgramID,
	}
	dbRes := pis.repository.Update(&updateProgramImg, filename)
	return dbRes.Error
}

func NewProgramImgService(repository repository.ProgramImgRepository) ProgramImgService {
	return &programImgService{
		repository: repository,
	}
}
