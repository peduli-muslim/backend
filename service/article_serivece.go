package service

import (
	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type ArticleService interface {
	CreateArticle(article dto.AddArticle) (entity.Article, error)
	GetOne(uuid uuid.UUID) (entity.Article, error)
	Update(article dto.UpdateArticle, uuid uuid.UUID) error
	GetAll() ([]entity.Article, error)
}

type articleService struct {
	repository      repository.ArticleRepository
	imageRepository repository.ArticleImgRepository
}

func (ps *articleService) CreateArticle(article dto.AddArticle) (entity.Article, error) {
	articleCreate := entity.Article{
		Judul:    article.Judul,
		Kategori: article.Kategori,
		Artikel:  article.Artikel,
	}
	dbRes := ps.repository.CreateArticle(&articleCreate)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Create Article", nil)
		return articleCreate, customErr
	}
	// for _, img := range article.Gambar {
	// 	i := &entity.ArticleImage{
	// 		ArticleID: articleCreate.UUID,
	// 	}
	// 	f, err := uuid.Parse(img["uuid"])
	// 	if err != nil {
	// 		customErr := helper.NewError(400, "Invalid uuid", nil)
	// 		return customErr
	// 	}
	// 	ps.imageRepository.Update(i, f)
	// }
	return articleCreate, nil
}

func (ps *articleService) GetOne(uuid uuid.UUID) (entity.Article, error) {
	article, dbRes := ps.repository.GetOne(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to get one Article", nil)
		return article, customErr
	}
	return article, nil
}

func (ps *articleService) Update(article dto.UpdateArticle, id uuid.UUID) error {
	articleUpdate := entity.Article{
		Judul:    article.Judul,
		Kategori: article.Kategori,
		Artikel:  article.Artikel,
	}
	dbRes := ps.repository.Update(articleUpdate, id)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Update Article", nil)
		return customErr
	}
	return nil
}

func (ps *articleService) GetAll() ([]entity.Article, error) {
	res, dbRes := ps.repository.GetAll()
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Get All Article", nil)
		return res, customErr
	}
	return res, nil
}

func NewArticleService(repository repository.ArticleRepository, imageRepository repository.ArticleImgRepository) ArticleService {
	return &articleService{
		repository:      repository,
		imageRepository: imageRepository,
	}
}
