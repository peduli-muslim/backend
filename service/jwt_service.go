package service

import (
	"fmt"
	"time"

	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

type JwtSerivce interface {
	GenerateToken(userID uuid.UUID, role string, grantType string) string
	// GenerateTokenFromGoogle(googleToken string) string
	ValidateToken(token string) (*jwt.Token, error)
}

type JwtCustomClaim struct {
	Identity  interface{} `json:"identity"`
	Role      string      `json:"role"`
	GrantType string      `json:"grant_type"`
	jwt.RegisteredClaims
}

type jwtService struct {
	secretKey string
}

func (js *jwtService) GenerateToken(userID uuid.UUID, role string, grantType string) string {
	issued := time.Now()
	var exp time.Time
	if grantType == "access_token" {
		exp = issued.Add(time.Hour * 3)
		// exp = issued.Add(time.Second * 10)
	} else {
		exp = issued.Add(time.Hour * 24 * 30)
	}
	claim := &JwtCustomClaim{
		userID,
		role,
		grantType,
		jwt.RegisteredClaims{
			ID:        uuid.NewString(),
			Issuer:    "Local",
			ExpiresAt: jwt.NewNumericDate(exp),
			IssuedAt:  jwt.NewNumericDate(issued),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	t, err := token.SignedString([]byte(js.secretKey))
	if err != nil {
		helper.NewError(422, "Failed to Generate Token", nil)
		panic(err)
	}
	return t
}

// func (js *jwtService) GenerateTokenFromGoogle(googleToken string) string {
// 	claim := &JwtCustomClaim{
// 		googleToken,
// 		"access_token",
// 		jwt.RegisteredClaims{
// 			Issuer:    "Google",
// 			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 3)),
// 			IssuedAt:  jwt.NewNumericDate(time.Now()),
// 		},
// 	}
// 	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
// 	t, err := token.SignedString([]byte(js.secretKey))
// 	if err != nil {
// 		panic(err)
// 	}
// 	return t
// }

func (js *jwtService) ValidateToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method %v", t.Header["alg"])
		}
		return []byte(js.secretKey), nil
	})
}

func NewJwtService(secretKey string) JwtSerivce {
	return &jwtService{
		secretKey: secretKey,
	}
}
