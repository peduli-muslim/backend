package service

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type DonaturService interface {
	CreateDonatur(donatur dto.CreateDonatur, programID uuid.UUID) error
	CheckProgram(programID uuid.UUID) (entity.Program, error)
	VerifikasiPembayaran(donatur dto.VerifyDonatur) error
	GetAllbyProgram(programID uuid.UUID) ([]dto.PublicDonatur, error)
}

type donaturService struct {
	donaturRepository repository.DonaturRepository
	programRepository repository.ProgramRepository
}

func (ds *donaturService) CreateDonatur(donatur dto.CreateDonatur, programID uuid.UUID) error {
	donaturCreate := entity.Donatur{
		Nama:      donatur.Nama,
		Anonim:    donatur.Anonim,
		NoTelp:    donatur.NoTelp,
		Email:     donatur.Email,
		Donasi:    donatur.Donasi,
		ProgramID: programID,
		Verified:  false,
	}
	program, err := ds.CheckProgram(programID)
	if err != nil {
		e := err.(*helper.CustomError)
		customErr := helper.NewError(e.Code, e.Message, nil)
		return customErr
	}
	if program.Status != "Open" {
		customErr := helper.NewError(400, "Program is Closed", nil)
		return customErr
	}
	dbRes := ds.donaturRepository.CreateDonatur(&donaturCreate)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to create Donatur", nil)
		return customErr
	}
	return nil
}

func (ds *donaturService) CheckProgram(programID uuid.UUID) (entity.Program, error) {
	res, dbRes := ds.programRepository.GetOneProgram(programID)
	return res, dbRes.Error
}

func (ds *donaturService) VerifikasiPembayaran(donatur dto.VerifyDonatur) error {
	uuid, err := uuid.Parse(donatur.UUID)
	if err != nil {
		customErr := helper.NewError(422, "Invalid donatur id", nil)
		return customErr
	}
	verifyDonatur, dbRes := ds.donaturRepository.GetOneDonatur(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Donatur not found", nil)
		return customErr
	}
	if verifyDonatur.Verified {
		customErr := helper.NewError(422, "Donatur verified", nil)
		return customErr
	}
	verifyDonatur.Verified = donatur.Verified
	dbRes = ds.donaturRepository.UpdateDonatur(verifyDonatur, uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to verify Donatur", nil)
		return customErr
	}
	program, dbRes := ds.programRepository.GetOneProgram(verifyDonatur.ProgramID)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Invalid program id", nil)
		return customErr
	}
	program.DanaTerkumpul += verifyDonatur.Donasi
	program.TotalDonatur++
	fmt.Println(program)
	dbRes = ds.programRepository.UpdateProgram(&program, program.UUID)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to update program", nil)
		return customErr
	}
	return nil
}

func (ds *donaturService) GetAllbyProgram(programID uuid.UUID) ([]dto.PublicDonatur, error) {
	donaturs, dbRes := ds.donaturRepository.GetAllbyProgram(programID)
	var res []dto.PublicDonatur
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to update program", nil)
		return res, customErr
	}
	for _, donatur := range donaturs {
		d := &dto.PublicDonatur{
			Nama:    donatur.Nama,
			Tanggal: donatur.UpdatedAt.Format("2006-01-02"),
			Nominal: donatur.Donasi,
		}
		if donatur.Anonim {
			d.Nama = "Anonim"
		}
		res = append(res, *d)
	}
	return res, nil
}

func NewDonaturService(donaturRepository repository.DonaturRepository, programRepository repository.ProgramRepository) DonaturService {
	return &donaturService{
		donaturRepository: donaturRepository,
		programRepository: programRepository,
	}
}
