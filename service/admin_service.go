package service

import (
	"fmt"
	"strconv"
	"time"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
)

type AdminService interface {
	// CreateAdmin(admin dto.CreateAdmin) (entity.Admin, string, error)
	CreateAdmin(admin dto.CreateAdmin) (entity.Admin, error)
	GetOne(uuid uuid.UUID) (entity.Admin, error)
	GetByEmail(email string) (entity.Admin, error)
	// Login(login dto.Login) (entity.Admin, error)
	// UpdatePassword(password dto.UpdatePassword, uuid uuid.UUID) error
	SetGIDNama(gid string, nama string, uuid uuid.UUID) error
	UpdateAdmin(admin dto.UpdateAdmin, uuid uuid.UUID) error
	UpdateAdminSelf(admin dto.UpdateAdminSelf, uuid uuid.UUID) error
	GetAll() ([]entity.Admin, error)
	GetOnebyGID(gid string) (entity.Admin, error)
}

type adminService struct {
	repository repository.AdminRepository
}

func (as *adminService) CreateAdmin(admin dto.CreateAdmin) (entity.Admin, error) {
	_, dbRes := as.repository.GetByEmail(admin.Email)
	if dbRes.Error != nil {
		adminCreate := entity.Admin{
			Nama:  fmt.Sprintf("admin%s", strconv.FormatInt(time.Now().Unix(), 10)),
			Email: admin.Email,
			Super: admin.Super,
		}
		dbRes := as.repository.CreateAdmin(&adminCreate)
		if dbRes.Error != nil {
			customErr := helper.NewError(422, "Failed to Create Admin", nil)
			return adminCreate, customErr
		}
		return adminCreate, nil
	}
	customErr := helper.NewError(400, "Email exis", nil)
	return entity.Admin{}, customErr
}

func (as *adminService) GetOne(uuid uuid.UUID) (entity.Admin, error) {
	admin, dbRes := as.repository.GetOne(uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Get One Admin", nil)
		return admin, customErr
	}
	return admin, nil
}

func (as *adminService) GetByEmail(email string) (entity.Admin, error) {
	admin, dbRes := as.repository.GetByEmail(email)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Get One Admin", nil)
		return admin, customErr
	}
	return admin, nil
}

// func (as *adminService) Login(login dto.Login) (entity.Admin, error) {
// 	admin, dbRes := as.repository.GetByUsername(login.Username)
// 	if dbRes.Error != nil {
// 		customErr := helper.NewError(403, "Username or Password wrong", nil)
// 		return admin, customErr
// 	}
// 	if err := bcrypt.CompareHashAndPassword(
// 		[]byte(admin.Password),
// 		[]byte(login.Password),
// 	); err != nil {
// 		customErr := helper.NewError(403, "Username or Password wrong", nil)
// 		return admin, customErr
// 	}
// 	return admin, nil
// }

// func (as *adminService) UpdatePassword(password dto.UpdatePassword, uuid uuid.UUID) error {
// 	if password.NewPassword == password.OldPassword {
// 		customErr := helper.NewError(400, "Old and New Password same", nil)
// 		return customErr
// 	}
// 	admin, err := as.GetOne(uuid)
// 	if err != nil {
// 		customErr := helper.NewError(400, "User not found", nil)
// 		return customErr
// 	}
// 	err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(password.OldPassword))
// 	if err != nil {
// 		customErr := helper.NewError(400, "Password wrong", nil)
// 		return customErr
// 	}
// 	newPass, err := bcrypt.GenerateFromPassword([]byte(password.NewPassword), 10)
// 	if err != nil {
// 		customErr := helper.NewError(422, "Failed to hash Password", nil)
// 		return customErr
// 	}
// 	admin.Password = string(newPass)
// 	dbRes := as.repository.UpdateAdmin(&admin, uuid)
// 	if dbRes.Error != nil {
// 		customErr := helper.NewError(422, "Failed to update Password", nil)
// 		return customErr
// 	}
// 	return nil
// }

func (as *adminService) SetGIDNama(gid string, nama string, uuid uuid.UUID) error {
	adminUpdate := entity.Admin{
		GID:  gid,
		Nama: nama,
	}
	dbRes := as.repository.UpdateAdmin(&adminUpdate, uuid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to update Admin", nil)
		return customErr
	}
	return nil
}

func (as *adminService) UpdateAdmin(admin dto.UpdateAdmin, uuid uuid.UUID) error {
	_, dbRes := as.repository.GetByEmail(admin.Email)
	if dbRes.Error != nil {
		adminUpdate := entity.Admin{
			Email: admin.Email,
			Super: admin.Super,
		}
		dbRes := as.repository.UpdateAdmin(&adminUpdate, uuid)
		if dbRes.Error != nil {
			customErr := helper.NewError(422, "Failed to update Admin", nil)
			return customErr
		}
		return nil
	}
	customErr := helper.NewError(422, "Email exis", nil)
	return customErr
}

func (as *adminService) UpdateAdminSelf(admin dto.UpdateAdminSelf, uuid uuid.UUID) error {
	adminDB, dbRes := as.repository.GetByEmail(admin.Email)
	fmt.Println(adminDB)
	fmt.Println(dbRes.Error != nil || adminDB.Email == admin.Email)
	if dbRes.Error != nil || adminDB.Email == admin.Email {
		// adminDB, dbRes = as.repository.GetByUsername(admin.Username)
		// if dbRes.Error != nil || adminDB.Username == admin.Username {
		// if dbRes.Error != nil {
		adminUpdate := entity.Admin{
			Nama:  admin.Nama,
			Email: admin.Email,
		}
		dbRes := as.repository.UpdateAdmin(&adminUpdate, uuid)
		if dbRes.Error != nil {
			customErr := helper.NewError(422, "Failed to update Admin", nil)
			return customErr
		}
		return nil
		// }

	}
	customErr := helper.NewError(422, "Email exis", nil)
	return customErr
}

func (as *adminService) GetAll() ([]entity.Admin, error) {
	admins, dbRes := as.repository.GetAll()
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Get All Admin", nil)
		return admins, customErr
	}
	return admins, dbRes.Error
}

func (as *adminService) GetOnebyGID(gid string) (entity.Admin, error) {
	user, dbRes := as.repository.GetOnebyGID(gid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "User not found", nil)
		return user, customErr
	}
	return user, nil
}

func NewAdminService(repository repository.AdminRepository) AdminService {
	return &adminService{
		repository: repository,
	}
}
