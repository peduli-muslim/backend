package service

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/dto"
	"github.com/MuShaf-NMS/peduli_muslim/entity"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/repository"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type AuthService interface {
	CreateAuth(auth dto.Register) (entity.Auth, error)
	UpdateUsername(auth dto.UpdateUsername, userID uuid.UUID) error
	UpdatePassword(auth dto.UpdatePassword, userID uuid.UUID) error
	CheckUser(username string) bool
	GetAuth(userID uuid.UUID) (entity.Auth, error)
	GetAuthWithRole(userID uuid.UUID) (interface{}, string, error)
	GetAuthByGIDWithRole(gid string) (interface{}, string, error)
	GetAdmin(adminID uuid.UUID) (repository.Admin, error)
	GetUser(userID uuid.UUID) (repository.User, error)
	GetAdminByEmail(email string) (repository.Admin, error)
	GetUserByEmail(email string) (repository.User, error)
	GetAdminByGID(gid string) (repository.Admin, error)
	GetUserByGID(gid string) (repository.User, error)
	LoginAdmin(login dto.Login) (repository.Admin, error)
	LoginUser(login dto.Login) (repository.User, error)
}

type authService struct {
	repository repository.AuthRepository
}

func (as *authService) CreateAuth(auth dto.Register) (entity.Auth, error) {
	// byteHashedPass, err := bcrypt.GenerateFromPassword([]byte(auth.Password), 10)
	// if err != nil {
	// 	customErr := helper.NewError(422, "Failed to hash password", nil)
	// 	return entity.Auth{}, customErr
	// }
	// hashedPass := string(byteHashedPass)
	authCreate := entity.Auth{
		UserID: auth.UserID,
		// Username: auth.Username,
		// Password: hashedPass,
	}
	dbRes := as.repository.CreateAuth(&authCreate)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Create Auth", nil)
		return authCreate, customErr
	}
	return authCreate, nil
}

func (as *authService) UpdateUsername(auth dto.UpdateUsername, userId uuid.UUID) error {
	updateAuth := entity.Auth{
		Username: auth.Username,
	}
	dbRes := as.repository.UpdateAuth(&updateAuth, userId)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Update Username", nil)
		return customErr
	}
	return nil
}

func (as *authService) UpdatePassword(password dto.UpdatePassword, userId uuid.UUID) error {
	auth, dbRes := as.repository.GetByUserID(userId)
	if dbRes.Error != nil {
		fmt.Println("UNF")
		customErr := helper.NewError(400, "User not found", nil)
		return customErr
	}
	fmt.Println(auth.Password)
	if auth.Password != "" {
		err := bcrypt.CompareHashAndPassword([]byte(auth.Password), []byte(password.OldPassword))
		if err != nil {
			fmt.Println("FCP")
			customErr := helper.NewError(400, "Password wrong", nil)
			return customErr
		}
	}
	fmt.Println(auth)
	byteHashedPass, err := bcrypt.GenerateFromPassword([]byte(password.NewPassword), 10)
	if err != nil {
		customErr := helper.NewError(422, "Failed to hash password", nil)
		return customErr
	}
	hashedPass := string(byteHashedPass)
	updateAuth := entity.Auth{
		Password: hashedPass,
	}
	fmt.Println(updateAuth)
	dbRes = as.repository.UpdateAuth(&updateAuth, userId)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Update Password", nil)
		return customErr
	}
	return nil
}

func (as *authService) CheckUser(username string) bool {
	_, dbRes := as.repository.GetByUsername(username)
	return dbRes.RowsAffected == 1
}

func (as *authService) GetAuth(userID uuid.UUID) (entity.Auth, error) {
	auth, dbRes := as.repository.GetByUserID(userID)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Failed to Get Auth", nil)
		return auth, customErr
	}
	return auth, nil
}
func (as *authService) GetAuthWithRole(userID uuid.UUID) (interface{}, string, error) {
	user, dbRes := as.repository.GetUser(userID)
	if dbRes.Error != nil {
		admin, dbRes := as.repository.GetAdmin(userID)
		if dbRes.Error != nil {
			customErr := helper.NewError(422, "Failed to Get Auth", nil)
			return user, "", customErr
		}
		return admin, "admin", nil
	}
	return user, "user", nil
}

func (as *authService) GetAuthByGIDWithRole(gid string) (interface{}, string, error) {
	user, dbRes := as.repository.GetUserByGID(gid)
	if dbRes.Error != nil {
		admin, dbRes := as.repository.GetAdminByGID(gid)
		if dbRes.Error != nil {
			customErr := helper.NewError(422, "Failed to Get Auth", nil)
			return user, "", customErr
		}
		return admin, "admin", nil
	}
	return user, "user", nil
}

func (as *authService) LoginAdmin(login dto.Login) (repository.Admin, error) {
	auth, dbRes := as.repository.GetAdminByUsername(login.Username)
	// auth, dbRes := as.repository.GetByUsername(login.Username)
	if dbRes.Error != nil {
		customErr := helper.NewError(403, "Username or Password wrong", nil)
		return auth, customErr
	}
	err := bcrypt.CompareHashAndPassword([]byte(auth.Password), []byte(login.Password))
	if err != nil {
		customErr := helper.NewError(403, "Username or Password wrong", nil)
		return auth, customErr
	}
	return auth, nil
}

func (as *authService) LoginUser(login dto.Login) (repository.User, error) {
	auth, dbRes := as.repository.GetUserByUsername(login.Username)
	// auth, dbRes := as.repository.GetByUsername(login.Username)
	if dbRes.Error != nil {
		customErr := helper.NewError(403, "Username or Password wrong", nil)
		return auth, customErr
	}
	err := bcrypt.CompareHashAndPassword([]byte(auth.Password), []byte(login.Password))
	if err != nil {
		customErr := helper.NewError(403, "Username or Password wrong", nil)
		return auth, customErr
	}
	return auth, nil
}

func (as *authService) GetAdmin(adminID uuid.UUID) (repository.Admin, error) {
	admin, dbRes := as.repository.GetAdmin(adminID)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Something wrong", nil)
		return admin, customErr
	}
	return admin, nil
}

func (as *authService) GetUser(userID uuid.UUID) (repository.User, error) {
	user, dbRes := as.repository.GetUser(userID)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Something wrong", nil)
		return user, customErr
	}
	return user, nil
}

func (as *authService) GetAdminByEmail(email string) (repository.Admin, error) {
	admin, dbRes := as.repository.GetAdminByEmail(email)
	if dbRes.Error != nil {
		customErr := helper.NewError(401, "Unauthorized", nil)
		return admin, customErr
	}
	return admin, nil
}
func (as *authService) GetUserByEmail(email string) (repository.User, error) {
	user, dbRes := as.repository.GetUserByEmail(email)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Something wrong", nil)
		return user, customErr
	}
	return user, nil
}

func (as *authService) GetAdminByGID(gid string) (repository.Admin, error) {
	admin, dbRes := as.repository.GetAdminByGID(gid)
	if dbRes.Error != nil {
		customErr := helper.NewError(401, "Unauthorized", nil)
		return admin, customErr
	}
	return admin, nil
}

func (as *authService) GetUserByGID(gid string) (repository.User, error) {
	user, dbRes := as.repository.GetUserByGID(gid)
	if dbRes.Error != nil {
		customErr := helper.NewError(422, "Something wrong", nil)
		return user, customErr
	}
	return user, nil
}

func NewAuthService(repository repository.AuthRepository) AuthService {
	return &authService{
		repository: repository,
	}
}
