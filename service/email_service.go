package service

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/config"
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"gopkg.in/gomail.v2"
)

type EmailService interface {
	Send(to string, subject string, body string) error
}

type emailService struct {
	host     string
	port     int
	username string
	password string
}

func (es *emailService) Send(to string, subject string, body string) error {
	msg := gomail.NewMessage()
	msg.SetHeader("From", es.username)
	msg.SetHeader("To", to)
	msg.SetHeader("Subject", subject)
	msg.SetBody("text/html", body)
	mail := gomail.NewDialer(es.host, es.port, es.username, es.password)
	err := mail.DialAndSend(msg)
	if err != nil {
		fmt.Println(err)
		customErr := helper.NewError(422, "Failed to send email", nil)
		return customErr
	}
	return nil
}

func NewEmailService(config config.Config) EmailService {
	return &emailService{
		host:     config.Mail_Host,
		port:     config.Mail_Port,
		username: config.MAIL_Username,
		password: config.MAIL_Password,
	}
}
