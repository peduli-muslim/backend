package service

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"strings"

	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/google/uuid"
)

type UploadImgService interface {
	Upload(file multipart.File, header *multipart.FileHeader) (string, error)
}

type uploadImgService struct {
	assetsDir  string
	assetsLink string
}

func (us *uploadImgService) Upload(file multipart.File, header *multipart.FileHeader) (string, error) {
	filename := header.Filename
	splitName := strings.Split(filename, ".")
	ext := splitName[len(splitName)-1]
	fmt.Println(ext)
	fmt.Println(header.Size)
	oldAddress := us.assetsDir + "/" + "img/" + filename
	f, err := os.Create(oldAddress)
	if err != nil {
		fmt.Println(err)
		customErr := helper.NewError(422, "Failed to Upload Image", nil)
		return "", customErr
	}
	defer f.Close()
	if _, err := io.Copy(f, file); err != nil {
		fmt.Println(err)
		customErr := helper.NewError(422, "Failed to Upload Image", nil)
		return "", customErr
	}
	uuid, err := uuid.NewRandom()
	if err != nil {
		fmt.Println(err)
		customErr := helper.NewError(422, "Failed to Upload Image", nil)
		return "", customErr
	}
	relativePath := "img/" + uuid.String() + "." + ext
	newAddress := us.assetsDir + "/" + relativePath
	fmt.Println(relativePath)
	if err := os.Rename(oldAddress, newAddress); err != nil {
		fmt.Println(err)
		customErr := helper.NewError(422, "Failed to Upload Image", nil)
		return "", customErr
	}
	url := us.assetsLink + "/" + relativePath
	return url, nil
}

func NewUploadImgService(assetsDir string, assetsLink string) UploadImgService {
	return &uploadImgService{
		assetsDir:  assetsDir,
		assetsLink: assetsLink,
	}
}
