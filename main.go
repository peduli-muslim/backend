package main

import (
	"github.com/MuShaf-NMS/peduli_muslim/config"
	"github.com/MuShaf-NMS/peduli_muslim/db"
	"github.com/MuShaf-NMS/peduli_muslim/router"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	config.LoadEnv()
	config := config.Conf
	server := gin.Default()
	db := db.InitializeDB(config)
	cors := cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE"},
	})
	server.Use(cors)
	router.InitializeRouter(server, db, config)
	server.Run(":" + config.App_Port)
}
