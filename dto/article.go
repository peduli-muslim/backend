package dto

import (
	"time"

	"github.com/google/uuid"
)

type AddArticle struct {
	Judul    string              `json:"judul" validate:"required"`
	Kategori string              `json:"kategori" validate:"required"`
	Gambar   []map[string]string `json:"gambar" validate:"required,min=1"`
	Artikel  string              `json:"artikel" validate:"required"`
}

type ResArtikel struct {
	UUID      uuid.UUID           `json:"uuid"`
	Judul     string              `json:"judul"`
	Kategori  string              `json:"kategori"`
	Gambar    []map[string]string `json:"gambar"`
	Artikel   string              `json:"artikel"`
	CreatedAt time.Time           `json:"created_at"`
}

type UpdateArticle struct {
	Judul         string              `json:"judul" validate:"required"`
	Kategori      string              `json:"kategori" validate:"required"`
	AddedGambar   []map[string]string `json:"added_gambar"`
	DeletedGambar []map[string]string `json:"deleted_gambar"`
	Artikel       string              `json:"artikel" validate:"required"`
}
