package dto

import "github.com/google/uuid"

type AddArticleImage struct {
	Src  string
	UUID uuid.UUID
}

type UpdateArticleImage struct {
	Src       string
	ArticleID uuid.UUID
}
