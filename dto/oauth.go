package dto

type GoogleUserInfo struct {
	ID           string `json:"id"`
	GivenName    string `json:"given_name"`
	FamilyiName  string `json:"family_name"`
	Name         string `json:"name"`
	Email        string `json:"email"`
	Locale       string `json:"locale"`
	Picture      string `json:"picture"`
	VerifieEmail bool   `json:"verified_email"`
}

type Code struct {
	Code string `json:"code" validate:"required"`
}

type GoogleTokenInfo struct {
	IssuedTo      string `json:"issued_to"`
	Audience      string `json:"audience"`
	UserID        string `json:"user_id"`
	Scope         string `json:"scope"`
	ExpiresIn     int    `json:"expires_in"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	AccessType    string `json:"access_type"`
}
