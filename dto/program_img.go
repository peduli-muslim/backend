package dto

import "github.com/google/uuid"

type AddProgramImage struct {
	Src  string
	UUID uuid.UUID
}

type UpdateProgramImage struct {
	Src       string
	ProgramID uuid.UUID
}
