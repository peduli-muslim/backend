package dto

// type CreateUser struct {
// 	GID  string `json:"gid"`
// 	Nama string `json:"nama"`
// 	// NoTelp string `json:"no_telp"`
// 	Email string `json:"email"`
// }

type CreateUser struct {
	GID      string `json:"gid"`
	Nama     string `json:"nama" validate:"required"`
	Username string `json:"username" validate:"required,min=5"`
	Password string `json:"password" validate:"required,min=8"`
	NoTelp   string `json:"no_telp" validate:"required"`
	Email    string `json:"email" validate:"required"`
}

type UpdateUser struct {
	Nama   string `json:"nama" validate:"required"`
	NoTelp string `json:"no_telp" validate:"required"`
	Email  string `json:"email" validate:"required"`
}
