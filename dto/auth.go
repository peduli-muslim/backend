package dto

import "github.com/google/uuid"

type Register struct {
	UserID uuid.UUID `json:"userID"`
	// Nama     string    `json:"nama" validate:"required" gorm:"-"`
	Username string `json:"username"`
	Password string `json:"password"`
	// Email    string `json:"email"`
}

type Login struct {
	Username   string `json:"username" validate:"required,min=5"`
	Password   string `json:"password" validate:"required,min=8"`
	RememberMe bool   `json:"rememberMe"`
}

type UpdateUsername struct {
	Username string `json:"username" validate:"required,min=5"`
}

type UpdatePassword struct {
	OldPassword string `json:"old_password" validate:"omitempty,min=8"`
	NewPassword string `json:"new_password" validate:"required,min=8"`
}
