package dto

type CreateDonatur struct {
	Nama   string `json:"nama" validate:"required"`
	Anonim bool   `json:"anonim" validate:""`
	NoTelp string `json:"no_telp" validate:"required"`
	Email  string `json:"email" validate:"omitempty,email"`
	Donasi uint   `json:"donasi" validate:"required"`
	// ProgramID uuid.UUID `json:"program_id" validate:"required,uuid4_rfc4122"`
}

type VerifyDonatur struct {
	UUID     string `json:"uuid" validate:"required,uuid4_rfc4122"`
	Verified bool   `json:"verified" validate:"required"`
}

type PublicDonatur struct {
	Nama    string `json:"nama"`
	Tanggal string `json:"tanggal"`
	Nominal uint   `json:"nominal"`
}
