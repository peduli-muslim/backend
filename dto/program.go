package dto

type CreateProgram struct {
	Nama       string `json:"nama" validate:"required"`
	Kategori   string `json:"kategori" validate:"required"`
	Target     uint   `json:"target" validate:"required,numeric"`
	BukaSampai string `json:"buka_sampai" validate:"required,datetime=2006-01-02"`
	Image      string `json:"image" validate:"required"`
	Rangkuman  string `json:"rangkuman"`
}

type UpdateProgram struct {
	Nama       string `json:"nama" validate:"required"`
	Kategori   string `json:"kategori" validate:"required"`
	Target     uint   `json:"target" validate:"required,numeric"`
	Status     string `json:"status" validate:"required"`
	BukaSampai string `json:"buka_sampai" validate:"required,datetime=2006-01-02"`
	Rangkuman  string `json:"rangkuman"`
}
