package dto

type CreateAdmin struct {
	// Username string `gorm:"size:50" json:"username" validate:"required,min=5"`
	// Password string `gorm:"size:255" json:"password" validate:"required,alphanum,min=8"`
	Email string `json:"email" validate:"required,email"`
	Super bool   `json:"super"`
}

type UpdateAdmin struct {
	Email string `json:"email" validate:"email"`
	Super bool   `json:"super"`
}

type UpdateAdminSelf struct {
	Nama string `json:"nama"`
	// Password string `json:"password" validate:"min=8"`
	Email string `json:"email" validate:"email"`
}
