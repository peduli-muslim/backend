package config

import (
	"fmt"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func getEnv(key string, defaultVal string) string {
	if value, ok := os.LookupEnv(key); ok {
		fmt.Println(value)
		return value
	}
	return defaultVal
}

type Config struct {
	App_Port      string
	Env           string
	SecretKey     string
	Dialect       string
	DB_Host       string
	DB_User       string
	DB_Pass       string
	DB_Name       string
	DB_Port       string
	Asstes_Dir    string
	Asstes_Link   string
	Mail_Host     string
	Mail_Port     int
	MAIL_Username string
	MAIL_Password string
}

var Conf = &Config{
	App_Port:      "8000",
	Env:           "development",
	SecretKey:     "SECRET",
	Dialect:       "mysql",
	DB_Host:       "localhost",
	DB_User:       "root",
	DB_Pass:       "root",
	DB_Name:       "db_name",
	DB_Port:       "3306",
	Asstes_Dir:    "assets_dir",
	Asstes_Link:   "assets_link",
	Mail_Host:     "smtp.gmail.com",
	Mail_Port:     587,
	MAIL_Username: "example@gmail.com",
	MAIL_Password: "example",
}

func LoadEnv() {
	fmt.Println("Initial Environment")
	err := godotenv.Load()
	if err != nil {
		panic("Error .env file")
	}
	Conf.App_Port = getEnv("APP_PORT", Conf.App_Port)
	Conf.Env = getEnv("APP_ENV", Conf.Env)
	Conf.SecretKey = getEnv("ENV", Conf.SecretKey)
	Conf.Dialect = getEnv("DIALECT", Conf.Dialect)
	Conf.DB_Host = getEnv("DB_HOST", Conf.DB_Host)
	Conf.DB_User = getEnv("DB_USER", Conf.DB_User)
	Conf.DB_Pass = getEnv("DB_PASS", Conf.DB_Pass)
	Conf.DB_Name = getEnv("DB_NAME", Conf.DB_Name)
	Conf.DB_Port = getEnv("DB_PORT", Conf.DB_Port)
	Conf.Asstes_Dir = getEnv("ASSETS_DIR", Conf.Asstes_Dir)
	Conf.Asstes_Link = getEnv("ASSETS_LINK", Conf.Asstes_Dir)
	Conf.Mail_Host = getEnv("MAIL_HOST", Conf.Mail_Host)
	mailPort, _ := strconv.Atoi(getEnv("MAIL_PORT", strconv.Itoa(Conf.Mail_Port)))
	Conf.Mail_Port = mailPort
	Conf.MAIL_Username = getEnv("MAIL_USERNAME", Conf.MAIL_Username)
	Conf.MAIL_Password = getEnv("MAIL_PASSWORD", Conf.MAIL_Password)
}

var OAuthConfig = oauth2.Config{
	ClientID:     getEnv("GOOGLE_CLIENT_ID", ""),
	ClientSecret: getEnv("GOOGLE_CLIENT_SECRET", ""),
	Endpoint:     google.Endpoint,
	// RedirectURL:  "http://localhost:8000/google/callback",
	// RedirectURL: "http://localhost:8080",
	RedirectURL: "postmessage",
	Scopes: []string{
		"openid",
		"email",
		"profile",
	},
}
