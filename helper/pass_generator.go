package helper

import (
	"math/rand"
	"strings"
	"time"
)

// Helper to generate ramdom passowrd with spesifed length
func PassowrdGenerator(length int) string {
	rand.Seed(time.Now().Unix())
	charSet := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
	var res strings.Builder
	for i := 0; i < length; i++ {
		random := rand.Intn(len(charSet))
		randomChar := charSet[random]
		res.WriteString(string(randomChar))
	}
	return res.String()
}
