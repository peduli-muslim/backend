package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Article struct {
	Id        uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"<-:create;type:char(36);not null" json:"uuid"`
	Judul     string    `gorm:"size:150" json:"judul"`
	Artikel   string    `gorm:"text" json:"artikel"`
	Kategori  string    `gorm:"size:150" json:"kategori"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
	UpdatedAt time.Time `gorm:"date" json:"updated_at"`
}

func (posts *Article) BeforeCreate(tx *gorm.DB) error {
	var err error
	posts.UUID, err = uuid.NewRandom()
	if err != nil {
		return err
	}
	return err
}
