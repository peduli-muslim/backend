package entity

import (
	"time"

	"github.com/MuShaf-NMS/peduli_muslim/config"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ArticleImage struct {
	Id        uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"<-:create;type:char(36);not null" json:"uuid"`
	Src       string    `gorm:"size(255)" json:"-"`
	Url       string    `gorm:"-" json:"url"`
	ArticleID uuid.UUID `gorm:"type:char(36);not null" json:"article_id"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
	UpdatedAt time.Time `gorm:"date" json:"updated_at"`
}

func (articleImage *ArticleImage) AfterFind(tx *gorm.DB) error {
	var err error
	articleImage.Url = config.Conf.Asstes_Link + "/" + articleImage.Src
	return err
}
