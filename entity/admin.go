package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Admin struct {
	Id        uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"type:char(36);not null" json:"uuid"`
	GID       string    `gorm:"size:30" json:"-"`
	Nama      string    `gorm:"size:150" json:"nama"`
	Email     string    `gorm:"size:150" json:"email"`
	Super     bool      `gorm:"bool;default:false" json:"super"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
	UpdatedAt time.Time `gorm:"date" json:"updated_at"`
}

func (admin *Admin) BeforeCreate(scope *gorm.DB) error {
	var err error
	admin.UUID, err = uuid.NewRandom()
	if err != nil {
		return err
	}
	return err
}
