package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Donatur struct {
	Id        uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"type:char(36);not null" json:"uuid"`
	Nama      string    `gorm:"size:150" json:"nama"`
	Anonim    bool      `gorm:"bool" json:"anonim"`
	NoTelp    string    `gorm:"size:20" json:"no_telp"`
	Email     string    `gorm:"size:150" json:"email"`
	Donasi    uint      `gorm:"type:integer" json:"dana_terkumpul"`
	Verified  bool      `gorm:"bool;default:false" json:"verified"`
	ProgramID uuid.UUID `gorm:"type:char(36);not null" json:"program_id"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
	UpdatedAt time.Time `gorm:"date" json:"updated_at"`
}

func (donatur *Donatur) BeforeCreate(scope *gorm.DB) error {
	var err error
	donatur.UUID, err = uuid.NewRandom()
	if err != nil {
		return err
	}
	return err
}
