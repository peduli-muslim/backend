package entity

import (
	"time"

	"github.com/MuShaf-NMS/peduli_muslim/config"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Program struct {
	Id            uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID          uuid.UUID `gorm:"<-:create;type:char(36);not null" json:"uuid"`
	Nama          string    `gorm:"size:150" json:"nama"`
	Kategori      string    `gorm:"size:100" json:"kategori"`
	Rangkuman     string    `gorm:"text" json:"rangkuman"`
	Image         string    `gorm:"size:150" json:"-"`
	ImageSrc      string    `gorm:"-" json:"image"`
	DanaTerkumpul uint      `gorm:"type:bigint" json:"dana_terkumpul"`
	Target        uint      `gorm:"type:bigint" json:"target"`
	Persentase    float64   `gorm:"-" json:"persentase"`
	TotalDonatur  uint      `gorm:"type:smallint" json:"total_donatur"`
	Status        string    `gorm:"size:30;default:Open" json:"status"`
	BukaSampai    time.Time `gorm:"date" json:"buka_sampai"`
	CreatedAt     time.Time `gorm:"date" json:"created_at"`
	UpdatedAt     time.Time `gorm:"date" json:"updated_at"`
}

func (program *Program) BeforeCreate(tx *gorm.DB) error {
	var err error
	program.UUID, err = uuid.NewRandom()
	if err != nil {
		return err
	}
	return err
}

func (program *Program) AfterFind(tx *gorm.DB) error {
	var err error
	program.Persentase = float64(program.DanaTerkumpul) / float64(program.Target) * 100
	program.ImageSrc = config.Conf.Asstes_Link + "/" + program.Image
	return err
}
