package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	Id        uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"type:char(36);not null" json:"uuid"`
	GID       string    `gorm:"size:30" json:"-"`
	Nama      string    `gorm:"size:150" json:"nama"`
	NoTelp    string    `gorm:"size:20" json:"no_telp"`
	Email     string    `gorm:"size:150" json:"email"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
	UpdatedAt time.Time `gorm:"date" json:"updated_at"`
}

func (ser *User) BeforeCreate(scope *gorm.DB) error {
	var err error
	ser.UUID, err = uuid.NewRandom()
	if err != nil {
		return err
	}
	return err
}
