package entity

import (
	"github.com/google/uuid"
)

type Auth struct {
	Id       uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UserID   uuid.UUID `gorm:"type:char(36);not null" json:"userID"`
	Username string    `gorm:"size:50" json:"username"`
	Password string    `gorm:"size:255" json:"-"`
}
