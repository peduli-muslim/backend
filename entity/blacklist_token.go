package entity

import (
	"time"
)

type BlacklistToken struct {
	Id        uint      `gorm:"primaryKey;autoIncrement"`
	JTI       string    `gorm:"type:char(36);not null" json:"jti"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
}
