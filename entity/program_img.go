package entity

import (
	"time"

	"github.com/google/uuid"
)

type ProgramImage struct {
	Id        uint      `gorm:"primaryKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"<-:create;type:char(36);not null" json:"uuid"`
	Src       string    `gorm:"size(255)" json:"url"`
	ProgramID uuid.UUID `gorm:"type:char(36);not null" json:"program_id"`
	CreatedAt time.Time `gorm:"date" json:"created_at"`
	UpdatedAt time.Time `gorm:"date" json:"updated_at"`
}
