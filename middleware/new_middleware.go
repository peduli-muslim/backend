package middleware

import (
	"fmt"
	"strings"

	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

// Middleware to check if the request has token
func AuthorizeMiddlewareNew() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		auth := ctx.Request.Header.Get("Authorization")
		if len(auth) == 0 {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		splitAuth := strings.Split(auth, " ")
		if len(splitAuth) < 2 {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		if splitAuth[0] != "Bearer" {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		tokenString := splitAuth[1]
		ctx.Set("token", tokenString)
		// ctx.Next()
	}
}

// Middleware for local authentication
// If authentication fail then throw it to next authentication middleware
func LocalUserMiddlewareNew(jwtService service.JwtSerivce, authService service.AuthService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			fmt.Println(tokenGet)
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			// if local authentication fails then throw it to google authentication
			if err != nil || !token.Valid {
				ctx.Next()
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			fmt.Println(claim)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var userID uuid.UUID
			userID, err = uuid.Parse(claim["identity"].(string))
			if err != nil {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			_, err = authService.GetUser(userID)
			if err != nil {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			ctx.Set("userID", userID)
			ctx.Set("tokenClaim", claim)
			// ctx.Next()
		}
	}
}

func GoogleUserMiddlewareNew(oauthService service.OAuthService, authService service.AuthService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if _, exist := ctx.Get("userID"); exist {
			ctx.Next()
			return
		}
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := oauthService.GetTokenInfo(tokenString)
			if err != nil || token.ExpiresIn == 0 {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			auth, err := authService.GetUserByGID(token.UserID)
			if err != nil {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			ctx.Set("userID", auth.UserID)
			fmt.Println(ctx.Get("userID"))
			// ctx.Set("role", role)
			// ctx.Next()
		}
	}
}

// Middleware for refresh local authentication
func RefreshUserMiddlewareNew(jwtService service.JwtSerivce, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			if err != nil || !token.Valid {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var userID uuid.UUID
			userID, err = uuid.Parse(claim["identity"].(string))
			grantType := claim["grant_type"].(string)
			if err != nil || grantType != "refresh_token" {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.JSON(401, res)
				return
			}
			ctx.Set("userID", userID)
			ctx.Set("tokenClaim", claim)
			// ctx.Next()
		}
	}
}

// Middleware for admin authentication
// func AdminMiddlewareNew(jwtService service.JwtSerivce, adminService service.AdminService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
func LocalAdminMiddlewareNew(jwtService service.JwtSerivce, authService service.AuthService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			fmt.Println(tokenGet)
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			// if local authentication fails then throw it to google authentication
			if err != nil || !token.Valid {
				ctx.Next()
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			fmt.Println(claim)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var adminID uuid.UUID
			adminID, err = uuid.Parse(claim["identity"].(string))
			if err != nil {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			_, err = authService.GetAdmin(adminID)
			if err != nil {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			ctx.Set("adminID", adminID)
			ctx.Set("tokenClaim", claim)
			// ctx.Next()
		}
		// res := helper.ErrorResponseBuilder("Unauthorized", nil)
		// ctx.AbortWithStatusJSON(401, res)
	}
}

func GoogleAdminMiddlewareNew(oauthService service.OAuthService, authService service.AuthService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if _, exist := ctx.Get("adminID"); exist {
			ctx.Next()
			return
		}
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := oauthService.GetTokenInfo(tokenString)
			if err != nil || token.ExpiresIn == 0 {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			// auth, role, err := authService.GetAuthByGIDWithRole(token.UserID)
			auth, err := authService.GetAdminByGID(token.UserID)
			if err != nil {
				res := helper.ErrorResponseBuilder("Access denied", nil)
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			ctx.Set("adminID", auth.UserID)
			fmt.Println(ctx.Get("adminID"))
			// ctx.Set("role", role)
			// ctx.Next()
		}
	}
}

// Middleware for superadmin authentication
func SuperAdminMiddlewareNew(jwtService service.JwtSerivce, adminService service.AdminService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if adminIDGet, ok := ctx.Get("adminID"); ok {
			adminID := adminIDGet.(uuid.UUID)
			admin, _ := adminService.GetOne(adminID)
			if !admin.Super {
				res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			ctx.Next()
			return
		}
		res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
		ctx.AbortWithStatusJSON(403, res)
	}
}

// Middleware for refresh admin authentication
func RefreshAdminMiddlewareNew(jwtService service.JwtSerivce, adminService service.AdminService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			if err != nil || !token.Valid {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var adminID uuid.UUID
			adminID, err = uuid.Parse(claim["identity"].(string))
			grantType := claim["grant_type"]
			if err != nil || grantType != "refresh_token" {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			if _, err = adminService.GetOne(adminID); err != nil {
				res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			ctx.Set("adminID", adminID)
			ctx.Next()
			return
		}
		res := helper.ErrorResponseBuilder("Unauthorized", nil)
		ctx.AbortWithStatusJSON(401, res)
	}
}

// func AdminGoogleMiddlewareNew(oauthService service.OAuthService, adminService service.AdminService) gin.HandlerFunc {
// 	return func(ctx *gin.Context) {
// 		if _, exist := ctx.Get("adminID"); exist {
// 			ctx.Next()
// 			return
// 		}
// 		if tokenGet, ok := ctx.Get("token"); ok {
// 			tokenString := tokenGet.(string)
// 			token, err := oauthService.GetTokenInfo(tokenString)
// 			if err != nil || token.ExpiresIn == 0 {
// 				msg := map[string]string{"msg": "Invalid token"}
// 				res := helper.ErrorResponseBuilder("Unauthorized", msg)
// 				ctx.AbortWithStatusJSON(401, res)
// 				return
// 			}
// 			admin, err := adminService.GetOnebyGID(token.UserID)
// 			if err != nil {
// 				msg := map[string]string{"msg": "Invalid token"}
// 				res := helper.ErrorResponseBuilder("Unauthorized", msg)
// 				ctx.AbortWithStatusJSON(401, res)
// 				return
// 			}
// 			ctx.Set("adminID", admin.UUID)
// 			ctx.Next()
// 		}
// 	}
// }
