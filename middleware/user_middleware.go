package middleware

import (
	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

// Middleware for local authentication
// If authentication fail then throw it to next authentication middleware
func UserMiddleware(jwtService service.JwtSerivce, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			// if local authentication fails then throw it to google authentication
			if err != nil || !token.Valid {
				ctx.Next()
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var userID uuid.UUID
			userID, err = uuid.Parse(claim["identity"].(string))
			if err != nil {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			ctx.Set("userID", userID)
			ctx.Set("tokenClaim", claim)
			ctx.Next()
		}
	}
}

// Middleware for refresh local authentication
func UserRefreshMiddleware(jwtService service.JwtSerivce, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			if err != nil || !token.Valid {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var userID uuid.UUID
			userID, err = uuid.Parse(claim["identity"].(string))
			grantType := claim["grant_type"].(string)
			if err != nil || grantType != "refresh_token" {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.JSON(401, res)
				return
			}
			ctx.Set("userID", userID)
			ctx.Set("tokenClaim", claim)
			ctx.Next()
		}
	}
}

func UserGoogleMiddleware(oauthService service.OAuthService, userService service.UserService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if _, exist := ctx.Get("userID"); exist {
			ctx.Next()
			return
		}
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := oauthService.GetTokenInfo(tokenString)
			if err != nil || token.ExpiresIn == 0 {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			user, err := userService.GetOnebyGID(token.UserID)
			if err != nil {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			ctx.Set("userID", user.UUID)
			ctx.Next()
		}
	}
}
