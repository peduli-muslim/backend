package middleware

import (
	"strings"

	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/gin-gonic/gin"
)

// Middleware to check if the request has token
func AuthorizeMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		auth := ctx.Request.Header.Get("Authorization")
		if len(auth) == 0 {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		splitAuth := strings.Split(auth, " ")
		if len(splitAuth) < 2 {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		if splitAuth[0] != "Bearer" {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		tokenString := splitAuth[1]
		ctx.Set("token", tokenString)
		ctx.Next()
	}
}
