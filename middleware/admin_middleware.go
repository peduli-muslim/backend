package middleware

import (
	"fmt"

	"github.com/MuShaf-NMS/peduli_muslim/helper"
	"github.com/MuShaf-NMS/peduli_muslim/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

// Middleware for admin authentication
// func AdminMiddleware(jwtService service.JwtSerivce, adminService service.AdminService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
func AdminMiddleware(jwtService service.JwtSerivce, adminService service.AuthService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			if err != nil || !token.Valid {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var adminID uuid.UUID
			adminID, err = uuid.Parse(claim["identity"].(string))
			if err != nil {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			t, _ := adminService.GetAdmin(adminID)
			fmt.Println(t)
			if _, err = adminService.GetAdmin(adminID); err != nil {
				res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			ctx.Set("adminID", adminID)
			ctx.Set("tokenClaim", claim)
			ctx.Next()
			return
		}
		res := helper.ErrorResponseBuilder("Unauthorized", nil)
		ctx.AbortWithStatusJSON(401, res)
	}
}

// Middleware for superadmin authentication
func SuperAdminMiddleware(jwtService service.JwtSerivce, adminService service.AdminService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if getAdminID, ok := ctx.Get("adminID"); ok {
			adminID := getAdminID.(uuid.UUID)
			// if tokenGet, ok := ctx.Get("token"); ok {
			// 	tokenString := tokenGet.(string)
			// token, err := jwtService.ValidateToken(tokenString)
			// if err != nil || !token.Valid {
			// 	res := helper.ErrorResponseBuilder("Unauthorized", nil)
			// 	ctx.AbortWithStatusJSON(401, res)
			// 	return
			// }
			// claim := token.Claims.(jwt.MapClaims)
			// if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
			// 	res := helper.ErrorResponseBuilder("Unauthorized", nil)
			// 	ctx.AbortWithStatusJSON(401, res)
			// 	return
			// }
			// var adminID uuid.UUID
			// adminID, err = uuid.Parse(claim["identity"].(string))
			// if err != nil {
			// 	res := helper.ErrorResponseBuilder("Unauthorized", nil)
			// 	ctx.AbortWithStatusJSON(401, res)
			// 	return
			// }
			admin, err := adminService.GetOne(adminID)
			if err != nil {
				res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			if !admin.Super {
				res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			// ctx.Set("adminID", adminID)
			// ctx.Set("tokenClaim", claim)
			ctx.Next()
			return
		}
		res := helper.ErrorResponseBuilder("Unauthorized", nil)
		ctx.AbortWithStatusJSON(401, res)
	}
}

// Middleware for refresh admin authentication
func AdminRefreshMiddleware(jwtService service.JwtSerivce, adminService service.AdminService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := jwtService.ValidateToken(tokenString)
			if err != nil || !token.Valid {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			claim := token.Claims.(jwt.MapClaims)
			if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			var adminID uuid.UUID
			adminID, err = uuid.Parse(claim["identity"].(string))
			grantType := claim["grant_type"]
			if err != nil || grantType != "refresh_token" {
				res := helper.ErrorResponseBuilder("Unauthorized", nil)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			if _, err = adminService.GetOne(adminID); err != nil {
				res := helper.ErrorResponseBuilder("Forbidden", "Access denied")
				ctx.AbortWithStatusJSON(403, res)
				return
			}
			ctx.Set("adminID", adminID)
			ctx.Next()
			return
		}
		res := helper.ErrorResponseBuilder("Unauthorized", nil)
		ctx.AbortWithStatusJSON(401, res)
	}
}

func AdminGoogleMiddleware(oauthService service.OAuthService, adminService service.AdminService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if _, exist := ctx.Get("adminID"); exist {
			ctx.Next()
			return
		}
		if tokenGet, ok := ctx.Get("token"); ok {
			tokenString := tokenGet.(string)
			token, err := oauthService.GetTokenInfo(tokenString)
			if err != nil || token.ExpiresIn == 0 {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			admin, err := adminService.GetOnebyGID(token.UserID)
			if err != nil {
				msg := map[string]string{"msg": "Invalid token"}
				res := helper.ErrorResponseBuilder("Unauthorized", msg)
				ctx.AbortWithStatusJSON(401, res)
				return
			}
			ctx.Set("adminID", admin.UUID)
			ctx.Next()
		}
	}
}
