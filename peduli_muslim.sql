--
-- PostgreSQL database dump
--

-- Dumped from database version 10.21
-- Dumped by pg_dump version 10.18 (Ubuntu 10.18-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admins; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.admins (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    g_id character varying(30),
    nama character varying(150),
    email character varying(150),
    super boolean DEFAULT false,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.admins OWNER TO shafa;

--
-- Name: admins_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.admins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admins_id_seq OWNER TO shafa;

--
-- Name: admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.admins_id_seq OWNED BY public.admins.id;


--
-- Name: article_images; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.article_images (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    src text,
    article_id character(36) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.article_images OWNER TO shafa;

--
-- Name: article_images_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.article_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_images_id_seq OWNER TO shafa;

--
-- Name: article_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.article_images_id_seq OWNED BY public.article_images.id;


--
-- Name: articles; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.articles (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    judul character varying(150),
    artikel text,
    kategori character varying(150),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.articles OWNER TO shafa;

--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_id_seq OWNER TO shafa;

--
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;


--
-- Name: auths; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.auths (
    id bigint NOT NULL,
    user_id character(36) NOT NULL,
    username character varying(50),
    password character varying(255)
);


ALTER TABLE public.auths OWNER TO shafa;

--
-- Name: auths_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.auths_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auths_id_seq OWNER TO shafa;

--
-- Name: auths_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.auths_id_seq OWNED BY public.auths.id;


--
-- Name: blacklist_tokens; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.blacklist_tokens (
    id bigint NOT NULL,
    jti character(36) NOT NULL,
    created_at timestamp with time zone
);


ALTER TABLE public.blacklist_tokens OWNER TO shafa;

--
-- Name: blacklist_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.blacklist_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blacklist_tokens_id_seq OWNER TO shafa;

--
-- Name: blacklist_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.blacklist_tokens_id_seq OWNED BY public.blacklist_tokens.id;


--
-- Name: donaturs; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.donaturs (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    nama character varying(150),
    anonim boolean,
    no_telp character varying(20),
    email character varying(150),
    donasi integer,
    verified boolean DEFAULT false,
    program_id character(36) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.donaturs OWNER TO shafa;

--
-- Name: donaturs_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.donaturs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.donaturs_id_seq OWNER TO shafa;

--
-- Name: donaturs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.donaturs_id_seq OWNED BY public.donaturs.id;


--
-- Name: program_images; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.program_images (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    src text,
    program_id character(36) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.program_images OWNER TO shafa;

--
-- Name: program_images_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.program_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.program_images_id_seq OWNER TO shafa;

--
-- Name: program_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.program_images_id_seq OWNED BY public.program_images.id;


--
-- Name: programs; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.programs (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    nama character varying(150),
    kategori character varying(100),
    rangkuman text,
    image character varying(150),
    dana_terkumpul bigint,
    target bigint,
    total_donatur smallint,
    status character varying(30) DEFAULT 'Open'::character varying,
    buka_sampai timestamp with time zone,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.programs OWNER TO shafa;

--
-- Name: programs_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.programs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.programs_id_seq OWNER TO shafa;

--
-- Name: programs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.programs_id_seq OWNED BY public.programs.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: shafa
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    uuid character(36) NOT NULL,
    g_id character varying(30),
    nama character varying(150),
    no_telp character varying(20),
    email character varying(150),
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.users OWNER TO shafa;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: shafa
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO shafa;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shafa
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: admins id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.admins ALTER COLUMN id SET DEFAULT nextval('public.admins_id_seq'::regclass);


--
-- Name: article_images id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.article_images ALTER COLUMN id SET DEFAULT nextval('public.article_images_id_seq'::regclass);


--
-- Name: articles id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);


--
-- Name: auths id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.auths ALTER COLUMN id SET DEFAULT nextval('public.auths_id_seq'::regclass);


--
-- Name: blacklist_tokens id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.blacklist_tokens ALTER COLUMN id SET DEFAULT nextval('public.blacklist_tokens_id_seq'::regclass);


--
-- Name: donaturs id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.donaturs ALTER COLUMN id SET DEFAULT nextval('public.donaturs_id_seq'::regclass);


--
-- Name: program_images id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.program_images ALTER COLUMN id SET DEFAULT nextval('public.program_images_id_seq'::regclass);


--
-- Name: programs id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.programs ALTER COLUMN id SET DEFAULT nextval('public.programs_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: admins; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.admins (id, uuid, g_id, nama, email, super, created_at, updated_at) FROM stdin;
6	d0452c0b-56d1-467f-a618-2bc99f3eb93c		admin1657092426	update.nurmuhammadshafa@gmail.com	f	2022-07-06 07:27:06.293114+00	2022-07-06 07:27:10.753949+00
5	9d664acc-1a51-487c-a2f8-1d0c64f541c0	114367525337129848489	Muhammad Shafa Update	self.nurmuhammadshafa@gmail.com	t	2022-06-21 05:06:16.997382+00	2022-07-06 07:27:10.79042+00
\.


--
-- Data for Name: article_images; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.article_images (id, uuid, src, article_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.articles (id, uuid, judul, artikel, kategori, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: auths; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.auths (id, user_id, username, password) FROM stdin;
12	9d664acc-1a51-487c-a2f8-1d0c64f541c0	mushaf	$2a$10$pBr1Mx9M1magxgKRUWPiMOfsnDoCXcgrsyIAl3LMioCxJOoVH1EeS
\.


--
-- Data for Name: blacklist_tokens; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.blacklist_tokens (id, jti, created_at) FROM stdin;
\.


--
-- Data for Name: donaturs; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.donaturs (id, uuid, nama, anonim, no_telp, email, donasi, verified, program_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: program_images; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.program_images (id, uuid, src, program_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: programs; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.programs (id, uuid, nama, kategori, rangkuman, image, dana_terkumpul, target, total_donatur, status, buka_sampai, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: shafa
--

COPY public.users (id, uuid, g_id, nama, no_telp, email, created_at, updated_at) FROM stdin;
\.


--
-- Name: admins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.admins_id_seq', 6, true);


--
-- Name: article_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.article_images_id_seq', 3, true);


--
-- Name: articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.articles_id_seq', 2, true);


--
-- Name: auths_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.auths_id_seq', 16, true);


--
-- Name: blacklist_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.blacklist_tokens_id_seq', 9, true);


--
-- Name: donaturs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.donaturs_id_seq', 3, true);


--
-- Name: program_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.program_images_id_seq', 1, true);


--
-- Name: programs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.programs_id_seq', 2, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: shafa
--

SELECT pg_catalog.setval('public.users_id_seq', 10, true);


--
-- Name: admins admins_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);


--
-- Name: article_images article_images_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.article_images
    ADD CONSTRAINT article_images_pkey PRIMARY KEY (id);


--
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- Name: auths auths_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.auths
    ADD CONSTRAINT auths_pkey PRIMARY KEY (id);


--
-- Name: blacklist_tokens blacklist_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.blacklist_tokens
    ADD CONSTRAINT blacklist_tokens_pkey PRIMARY KEY (id);


--
-- Name: donaturs donaturs_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.donaturs
    ADD CONSTRAINT donaturs_pkey PRIMARY KEY (id);


--
-- Name: program_images program_images_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.program_images
    ADD CONSTRAINT program_images_pkey PRIMARY KEY (id);


--
-- Name: programs programs_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT programs_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: shafa
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

